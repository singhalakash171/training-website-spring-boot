package com.example.trainingwebsite.enums;

public enum JobCurrentStatus {

    OPEN, CLOSED
}
