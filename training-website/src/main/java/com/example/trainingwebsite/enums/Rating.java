package com.example.trainingwebsite.enums;

public enum Rating {

    STAR1, STAR2, STAR3, STAR4, STAR5
}
