package com.example.trainingwebsite.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum FileSubCategory {

    MAIN,ICON,SYLLABUS,OTHERS,PROFILE_PIC;
}
