package com.example.trainingwebsite.enums;

public enum UserRole {
    ADMIN, LEAD_MANAGER,LEAD_ASSISTANT,USER,TRAINER;
}
