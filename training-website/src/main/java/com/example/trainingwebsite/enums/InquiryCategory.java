package com.example.trainingwebsite.enums;

public enum InquiryCategory {

    SELF, COMPANY
}
