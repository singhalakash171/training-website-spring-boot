package com.example.trainingwebsite.enums;

public enum SubscriptionCategory {

    JOBS, TRAINING, UPDATES
}
