package com.example.trainingwebsite.enums;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public enum FileCategory {

    TRAINING("careermaker-images"),TRAINING_MODULE("careermaker-images")
    ,FEEDBACK("careermaker-images"),USER("careermaker-images"),
    FEE_PROGRAM("careermaker-images"),WHY_CHOOSE("careermaker-images");

    public String bucketName;
}
