package com.example.trainingwebsite.enums;

public enum JobType {

    FULL_TIME, PART_TIME
}
