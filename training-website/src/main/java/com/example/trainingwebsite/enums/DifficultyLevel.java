package com.example.trainingwebsite.enums;

public enum DifficultyLevel {

    EASY, MEDIUM, HARD, VERY_HARD
}
