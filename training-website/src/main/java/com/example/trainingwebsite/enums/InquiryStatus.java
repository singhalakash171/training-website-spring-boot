package com.example.trainingwebsite.enums;

public enum InquiryStatus {

    NEW, IN_ACTIVE, IN_PROGRESS, FOLLOW_UP ,JOINED_TRAINING, ONBOARDED;

}
