package com.example.trainingwebsite.enums;

public enum InquiryActionType {

    FOLLOW_UP_CALL,FOLLOW_UP_EMAIL,STATUS_CHANGE,CONVERT_TO_STUDENT,ASSIGN_LEAD_MANAGER,REASSIGN_LEAD_MANAGER,
    UPDATE_STATUS;
}
