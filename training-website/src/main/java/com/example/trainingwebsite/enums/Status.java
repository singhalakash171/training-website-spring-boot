package com.example.trainingwebsite.enums;

public enum Status {

    DRAFT,IN_PROGRESS,IN_REVIEW,AVAILABLE;
}
