package com.example.trainingwebsite.enums;

public enum Gender {

    MALE, FEMALE, ND
}
