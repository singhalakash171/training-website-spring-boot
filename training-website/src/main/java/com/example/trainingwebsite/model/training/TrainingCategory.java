package com.example.trainingwebsite.model.training;

public enum TrainingCategory {

    GENERAL, MASTER_PROGRAM, CERTIFICATION
}
