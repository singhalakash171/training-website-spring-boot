package com.example.trainingwebsite.model.reviews;


import com.example.trainingwebsite.enums.Rating;
import com.example.trainingwebsite.enums.Gender;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "testimonials")
@Data
public class Testimonial {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String companyName;

    @Column
    private String description;

    @Enumerated(EnumType.STRING)
    private Rating rating;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String image;

}
