package com.example.trainingwebsite.model.training;

import com.example.trainingwebsite.enums.Status;
import com.example.trainingwebsite.model.user.User;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
public class TrainingProgram {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToMany(mappedBy = "trainingProgram", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<TrainingModule> trainingModules;

    @OneToMany(mappedBy = "trainingProgram", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<TrainingFeature> trainingFeatures;

    @OneToMany(mappedBy = "trainingProgram", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<TrainingFaq> trainingFaqs;

    @OneToMany(mappedBy = "trainingProgram", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<TrainingFeeProgram> trainingFeePrograms;

    @OneToMany(mappedBy = "trainingProgram", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<TrainingWhyChoose> trainingWhyChooses;

    @OneToMany(mappedBy = "trainingProgram", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<TrainingCriteria> trainingCriteriaList;

    @OneToMany(mappedBy = "trainingProgram", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<TrainingSchedule> trainingSchedules;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "training_user_mapping",
            joinColumns = {@JoinColumn(name = "training_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> trainers  = new HashSet<>();

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String duration;

    @Column(columnDefinition = "TEXT")
    private String overview;

    @Enumerated(EnumType.STRING)
    private TrainingCategory trainingCategory;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(columnDefinition = "boolean default true")
    private boolean active;

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer sequence;

    @Column
    private String syllabusFilePath;

    @Column
    private String mainImage;


}
