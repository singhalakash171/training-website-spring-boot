package com.example.trainingwebsite.model.questions;


import com.example.trainingwebsite.config.ElasticIndex;
import com.example.trainingwebsite.enums.DifficultyLevel;
import com.example.trainingwebsite.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "questions")
@Document(indexName = ElasticIndex.QUESTIONS)
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Field(type = FieldType.Keyword)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "question_module_id")
    private QuestionModule questionModule;

    @Column(columnDefinition = "TEXT")
    @Field(type = FieldType.Text)
    private String questionText;

    @Field(type = FieldType.Text)
    @Column(columnDefinition = "TEXT")
    private String standardAnswer;

    @Column(columnDefinition = "TEXT")
    private String interviewAnswer;

    @Column
    private Integer noOfTimesAsked;

    @Enumerated(EnumType.STRING)
    private DifficultyLevel difficultyLevel;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(columnDefinition = "boolean default true")
    @Field(type = FieldType.Auto)
    private boolean active;

}
