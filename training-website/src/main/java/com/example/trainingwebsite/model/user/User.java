package com.example.trainingwebsite.model.user;

import com.example.trainingwebsite.enums.UserRole;
import com.example.trainingwebsite.model.inquiry.Inquiry;
import com.example.trainingwebsite.model.inquiry.InquiryAction;
import com.example.trainingwebsite.model.training.TrainingProgram;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String firstName;

    private String middleName;

    private String lastName;

    @Column(name = "email_id",unique = true)
    private String emailId;

    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @Column(name = "password")
    private String password;

    @Column(name = "credentials_non_expired")
    private boolean isCredentialsNonExpired;

    @Column(name = "account_non_expired")
    private boolean isAccountNonExpired;

    @Column(name = "account_non_locked")
    private boolean isAccountNonLocked;

    @Column(name = "update_password_required")
    private boolean isUpdatePasswordRequired;

    private boolean isVerifiedAccount;

    private boolean isAdminRegistered;

    @Column(columnDefinition = "boolean default true")
    private boolean active;

    @Column
    String profilePic;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_details_id")
    UserDetails userDetails;

    @ManyToMany(mappedBy = "trainers",cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<TrainingProgram> trainingProgram = new HashSet<>();

    @OneToMany(mappedBy = "inquiryManager",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Inquiry> managedLeads;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<InquiryAction> inquiryActions;

}
