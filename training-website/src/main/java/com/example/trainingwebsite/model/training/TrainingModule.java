package com.example.trainingwebsite.model.training;

import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "training_module")
public class TrainingModule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "training_program_id")
    private TrainingProgram trainingProgram;

    @OneToMany(mappedBy = "trainingModule", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<TrainingTopic> trainingTopics;

    private String name;

    private String description;

    private String additionalData;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer sequence;

    private String image;

    public TrainingModule() {
    }

}
