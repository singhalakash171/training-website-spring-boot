package com.example.trainingwebsite.model.reviews;


import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "internetReviews")
@Data
public class InternetReviews {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String info;

    @Column
    private Date date;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    private String image;
}
