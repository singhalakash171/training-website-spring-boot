package com.example.trainingwebsite.model;

import com.example.trainingwebsite.enums.FileCategory;
import com.example.trainingwebsite.enums.FileSubCategory;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class FileData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Enumerated(EnumType.STRING)
    private FileCategory fileCategory;

    @Enumerated(EnumType.STRING)
    private FileSubCategory subCategory;

    private String name;

    private String bucketName;

    private String fileRelativePath;

    private String fileFullPath;



}
