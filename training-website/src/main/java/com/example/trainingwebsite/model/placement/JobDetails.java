package com.example.trainingwebsite.model.placement;


import com.example.trainingwebsite.enums.JobCategory;
import com.example.trainingwebsite.enums.JobCurrentStatus;
import com.example.trainingwebsite.enums.JobType;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "jobDetails")
@Data
public class JobDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String title;

    @Column
    private String companyWebsiteUrl;

    @Enumerated(EnumType.STRING)
    private JobType jobType;

    @Enumerated(EnumType.STRING)
    private JobCategory jobCategory;

    @Column
    private String eligibilityCriteria;

    @Column
    private int noOfVacancies;

    @Column
    private String location;

    @Column
    private String packageDetails;

    @Enumerated(EnumType.STRING)
    private JobCurrentStatus jobCurrentStatus;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @Enumerated(EnumType.STRING)
    private Status status;

}
