package com.example.trainingwebsite.model.inquiry;


import com.example.trainingwebsite.enums.InquiryCategory;
import com.example.trainingwebsite.enums.InquiryStatus;
import com.example.trainingwebsite.model.user.User;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "inquiry")
@Data
public class Inquiry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String firstName;

    @Column
    private String middleName;

    @Column
    private String lastName;

    @Column
    private String email;

    @Column
    private String phoneNumber;

    @Column
    private String interestedProgram;

    @Enumerated(EnumType.STRING)
    private InquiryCategory inquiryCategory;

    @Column
    private String comments;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @Enumerated(EnumType.STRING)
    private InquiryStatus inquiryStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User inquiryManager;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "inquiry")
    private List<InquiryAction> inquiryActions;

}
