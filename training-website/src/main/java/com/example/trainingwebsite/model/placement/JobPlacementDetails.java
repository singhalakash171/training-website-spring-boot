package com.example.trainingwebsite.model.placement;


import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "jobPlacementDetails")
@Data
public class JobPlacementDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String studentName;

    @Column
    private Date date;

    @Column
    private String companyName;

    @Column
    private String description;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @Enumerated(EnumType.STRING)
    private Status status;

}
