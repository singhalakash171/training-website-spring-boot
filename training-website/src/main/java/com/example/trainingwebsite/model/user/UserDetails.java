package com.example.trainingwebsite.model.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "user_details")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String firstName;

    private String middleName;

    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(columnDefinition = "boolean default false")
    private boolean isPhoneVerified;

    private String addressLine1;

    private String addressLine2;

    private Integer pinCode;

    @OneToOne(mappedBy = "userDetails", fetch = FetchType.EAGER,cascade = CascadeType.PERSIST)
    User user;
}
