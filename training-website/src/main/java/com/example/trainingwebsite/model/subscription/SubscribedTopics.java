package com.example.trainingwebsite.model.subscription;


import com.example.trainingwebsite.enums.SubscriptionCategory;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "subscribedTopics")
@Data
public class SubscribedTopics {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private SubscriptionCategory subscriptionCategory;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

}
