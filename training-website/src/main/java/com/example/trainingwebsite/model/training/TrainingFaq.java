package com.example.trainingwebsite.model.training;

import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "training_faq")
public class TrainingFaq {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String question;

    private String answer;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "training_program_id")
    private TrainingProgram trainingProgram;

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer sequence;

    public TrainingFaq() {
    }
}
