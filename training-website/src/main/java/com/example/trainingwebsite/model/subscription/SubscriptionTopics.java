package com.example.trainingwebsite.model.subscription;

import com.example.trainingwebsite.enums.SubscriptionCategory;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class SubscriptionTopics {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private SubscriptionCategory subscriptionCategory;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "subscription_details_id")
    private SubscriptionDetails subscriptionDetails;


}
