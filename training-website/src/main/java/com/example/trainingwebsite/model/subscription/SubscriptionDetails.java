package com.example.trainingwebsite.model.subscription;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class SubscriptionDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false)
    private String email;

    @OneToMany(mappedBy = "subscriptionDetails", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SubscriptionTopics> subscriptionTopics;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

}
