package com.example.trainingwebsite.model.training;

import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class TrainingTopic {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    private String description;

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer sequence;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "training_module_id")
    private TrainingModule trainingModule;

    public TrainingTopic() {
    }
}
