package com.example.trainingwebsite.model.blog;


import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "blogData")
@Data
public class BlogData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String writtenBy;

    @Column
    private Date date;

    @Column
    private String header;

    @Column
    private String description;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

}
