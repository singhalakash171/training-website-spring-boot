package com.example.trainingwebsite.model.training;

import com.example.trainingwebsite.model.user.User;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name = "trainingSchedule")
public class TrainingSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private Date trainingDate;

    @Column
    private String startTime;

    @Column
    private String endTime;

    @Column
    private String duration;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "training_program_id")
    TrainingProgram trainingProgram;


}
