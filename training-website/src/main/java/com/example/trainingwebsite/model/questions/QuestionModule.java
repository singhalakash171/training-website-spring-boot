package com.example.trainingwebsite.model.questions;

import com.example.trainingwebsite.enums.Status;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "question_module")
public class QuestionModule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Field(type = FieldType.Auto)
    private Integer id;

    @OneToMany(mappedBy = "questionModule", fetch = FetchType.LAZY)
    private List<Question> questions;

    private String name;

    private String description;

    private String additionalData;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(columnDefinition = "boolean default true")
    private boolean active;

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer sequence;

    public QuestionModule() {
    }

}
