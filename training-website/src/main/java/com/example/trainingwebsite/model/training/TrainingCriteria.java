package com.example.trainingwebsite.model.training;

import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "training_criteria")
@Data
public class TrainingCriteria {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String criteria;

    @Column
    private String criteriaDescription;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "training_program_id", unique = true)
    private TrainingProgram trainingProgram;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer sequence;

    @Enumerated(EnumType.STRING)
    private Status status;
}
