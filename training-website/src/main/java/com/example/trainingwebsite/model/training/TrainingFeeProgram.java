package com.example.trainingwebsite.model.training;

import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "training_fee_program")
@Data
public class TrainingFeeProgram {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String trainingFee;

    @Column
    private String discountedFee;

    @Column
    private String description;

    @Column
    private String features;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "training_program_id", unique = true)
    private TrainingProgram trainingProgram;

    @Column(columnDefinition = "boolean default true")
    private Boolean active;

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer sequence;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String image;
}
