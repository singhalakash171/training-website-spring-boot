package com.example.trainingwebsite.dto.training;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.Status;
import com.example.trainingwebsite.model.training.TrainingCategory;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TrainingProgramDto extends LovDto {

    TrainingCategory trainingCategory;

    Status status;

    @NotNull(message = "Sequence is required")
    Integer sequence;

    @NotEmpty(message = "Overview is required")
    String overview;

    private String syllabusFilePath;

    private String mainImage;
}
