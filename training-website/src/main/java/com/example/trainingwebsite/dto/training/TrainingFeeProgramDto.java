package com.example.trainingwebsite.dto.training;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class TrainingFeeProgramDto extends LovDto {

    @NotNull(message = "Sequence is required.")
    Integer sequence;

    Status status;

    @NotBlank(message = "Program fee is required.")
    String programFee;

    @NotBlank(message = "Discounted fee is required.")
    String discountedFee;

    @NotBlank(message = "Feature is required.")
    String features;
}
