package com.example.trainingwebsite.dto.training;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class TrainingFeatureDto{

    Integer id;

    Integer _id;

    @NotNull(message = "Sequence is required.")
    Integer sequence;

    Status status;

    @NotBlank(message = "Name is required.")
    String name;

    String description;
}
