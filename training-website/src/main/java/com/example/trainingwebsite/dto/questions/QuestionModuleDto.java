package com.example.trainingwebsite.dto.questions;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class QuestionModuleDto extends LovDto {

    Status status;

    @NotNull(message = "Sequence is required.")
    Integer sequence;

    public QuestionModuleDto() {
    }

}
