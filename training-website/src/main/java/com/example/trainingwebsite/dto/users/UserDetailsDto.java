package com.example.trainingwebsite.dto.users;
import lombok.Data;

@Data
public class UserDetailsDto extends UserDetailsLovDto{

    private String addressLine1;

    private String addressLine2;

    private Integer pinCode;

    private String profilePic;

}
