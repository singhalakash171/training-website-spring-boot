package com.example.trainingwebsite.dto.training;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class TrainingCriteriaDto{

    Integer id;

    Integer _id;

    @NotNull(message = "Sequence is required.")
    Integer sequence;

    @NotBlank(message = "Criteria name is required.")
    String criteriaName;

    @NotBlank(message = "Criteria description is required.")
    String criteriaDescription;

    Status status;
}
