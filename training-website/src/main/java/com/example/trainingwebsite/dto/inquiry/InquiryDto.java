package com.example.trainingwebsite.dto.inquiry;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.InquiryStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InquiryDto {

    Integer inquiryId;

    @NotBlank(message = "FirstName is required.")
    private String firstName;

    private String middleName;

    @NotBlank(message = "LastName is required.")
    private String lastName;

    @NotBlank(message = "EmailId is required.")
    private String emailId;

    @NotBlank(message = "PhoneNumber is required.")
    private String phoneNumber;

    @NotBlank(message = "InterestedProgram is required.")
    private String interestedProgram;

    private String comments;

    private LovDto inquiryManager;

    private LovDto inquiryAssistant;

    private InquiryStatus inquiryStatus;

    List<InquiryActionDetailsDto> inquiryActionDetailsDtoList;

}
