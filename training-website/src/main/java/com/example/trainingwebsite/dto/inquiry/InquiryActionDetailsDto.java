package com.example.trainingwebsite.dto.inquiry;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.InquiryActionType;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class InquiryActionDetailsDto {

    Integer id;

    LovDto inquiryDto;

    InquiryActionType inquiryActionType;

    String comments;

    LovDto actionUserDetails;
}
