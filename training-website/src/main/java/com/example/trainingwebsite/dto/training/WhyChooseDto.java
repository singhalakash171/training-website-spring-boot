package com.example.trainingwebsite.dto.training;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class WhyChooseDto extends LovDto {

    @NotNull(message = "Sequence is required.")
    Integer sequence;

    Status status;
}
