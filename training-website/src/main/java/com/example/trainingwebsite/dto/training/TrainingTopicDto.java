package com.example.trainingwebsite.dto.training;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TrainingTopicDto extends LovDto {

    private Integer sequence;

    private Status status;

    private boolean active;
}
