package com.example.trainingwebsite.dto.inquiry;

import com.example.trainingwebsite.enums.InquiryStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class InquiryUpdateDto {

    @NotNull(message = "Id is required.")
    private Integer id;

    private InquiryStatus inquiryStatus;

    private Integer inquiryManagerId;

    private Integer inquiryAssistantId;


}
