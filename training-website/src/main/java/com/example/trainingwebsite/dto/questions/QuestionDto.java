package com.example.trainingwebsite.dto.questions;

import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class QuestionDto {

    Integer id;

    @NotBlank(message = "Question is required.")
    String question;

    @NotBlank(message = "StandardAnswer is required.")
    String standardAnswer;

    String interviewAnswer;

    @NotBlank(message = "DifficultyLevel is required.")
    String difficultyLevel;

    Integer noOfTimesAsked;

    @NotNull(message = "ModuleId is required.")
    Integer moduleId;

    Status status;

}
