package com.example.trainingwebsite.dto.users;
import lombok.Data;
import javax.validation.constraints.NotBlank;

@Data
public class UserRegistrationDto {

    private Integer id;

    @NotBlank(message = "FirstName is required.")
    private String firstName;

    private String middleName;

    @NotBlank(message = "LastName is required.")
    private String lastName;

    @NotBlank(message = "EmailId is required.")
    private String emailId;

    @NotBlank(message = "PhoneNumber is required.")
    private String phoneNumber;

    @NotBlank(message = "Password is required.")
    private String password;

}
