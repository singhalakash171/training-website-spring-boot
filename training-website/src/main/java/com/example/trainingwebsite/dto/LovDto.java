package com.example.trainingwebsite.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LovDto {
    String _id;
    Integer id;
    String name;
    String description;

    public LovDto(Integer id,String name,String description){
        this.id = id;
        this._id = id+"";
        this.name = name;
        this.description= description;
    }
}
