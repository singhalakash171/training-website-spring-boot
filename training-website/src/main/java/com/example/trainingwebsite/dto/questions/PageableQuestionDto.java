package com.example.trainingwebsite.dto.questions;

import com.example.trainingwebsite.dto.PageParameters;
import lombok.Data;

import java.util.List;

@Data
public class PageableQuestionDto extends PageParameters {

    List<QuestionDto> questionDtos;


}
