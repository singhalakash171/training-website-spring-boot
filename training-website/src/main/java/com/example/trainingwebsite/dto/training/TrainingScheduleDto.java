package com.example.trainingwebsite.dto.training;

import com.example.trainingwebsite.dto.LovDto;
import lombok.Data;

import java.sql.Date;

@Data
public class TrainingScheduleDto extends LovDto {

    private Date date;

    private String startTime;

    private String endTime;

    private String duration;
}
