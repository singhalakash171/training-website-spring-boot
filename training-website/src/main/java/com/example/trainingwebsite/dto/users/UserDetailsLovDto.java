package com.example.trainingwebsite.dto.users;

import com.example.trainingwebsite.enums.UserRole;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserDetailsLovDto {

    private Integer id;

    @NotBlank(message = "FirstName is required.")
    private String firstName;

    private String middleName;

    @NotBlank(message = "Last is required.")
    private String lastName;

    private String phoneNumber;

    private UserRole userRole;

    private String emailId;

    private boolean isUpdatePasswordRequired;

    private boolean isVerifiedAccount;

    private boolean isAdminRegistered;


}
