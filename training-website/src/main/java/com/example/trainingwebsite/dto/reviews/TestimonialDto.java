package com.example.trainingwebsite.dto.reviews;
import com.example.trainingwebsite.enums.Rating;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TestimonialDto {

    private Integer id;

    @NotBlank(message = "Company name is required.")
    private String companyName;

    @NotBlank(message = "Description is required.")
    private String description;

    @NotBlank(message = "Rating is required")
    private Rating rating;

    private Status status;

    private String image;
}
