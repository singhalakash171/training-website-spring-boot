package com.example.trainingwebsite.dto.training;

import lombok.Data;

import java.util.List;

@Data
public class TrainingProgramDetailsDto{

    TrainingProgramDto trainingProgramDto;

    List<TrainingModuleDto> trainingModuleDtoList;

    List<TrainingFeatureDto> trainingFeatureDtos;

    List<TrainingFaqDto> trainingFaqQuestion;

    List<TrainingFeeProgramDto> trainingFeeProgramDtos;

    List<TrainingCriteriaDto> trainingCriteriaDtos;

}
