package com.example.trainingwebsite.dto.training;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class TrainingFaqDto extends LovDto {

    @NotBlank(message = "Faq question is required.")
    String question;

    @NotBlank(message = "Faq answer is required.")
    String answer;

    Status status;

    @NotNull(message = "Sequence is required.")
    Integer sequence;
}
