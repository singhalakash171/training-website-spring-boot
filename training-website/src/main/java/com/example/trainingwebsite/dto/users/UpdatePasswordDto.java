package com.example.trainingwebsite.dto.users;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
public class UpdatePasswordDto {

    Integer userId;

    String existingPassword;

    @NotBlank(message = "NewPassword is required.")
    String newPassword;

    @NotBlank(message = "Confirm Password is required.")
    String confirmPassword;

}
