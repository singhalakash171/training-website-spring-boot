package com.example.trainingwebsite.dto.users;

import com.example.trainingwebsite.enums.UserRole;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class UpdateUserRoleDto {

    @NotNull(message = "UserId is required.")
    private Integer userId;

    private UserRole userRole;

}
