package com.example.trainingwebsite.dto.training;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.enums.Status;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class TrainingModuleDto extends LovDto {

    Status status;

    @NotNull(message = "Sequence is required.")
    Integer sequence;

    boolean free;

    String image;

    List<TrainingTopicDto> trainingTopicDtoList;

    public TrainingModuleDto() {
        trainingTopicDtoList = new ArrayList<>();
    }

}
