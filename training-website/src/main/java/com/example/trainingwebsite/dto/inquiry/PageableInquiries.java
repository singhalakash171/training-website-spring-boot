package com.example.trainingwebsite.dto.inquiry;

import com.example.trainingwebsite.dto.PageParameters;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageableInquiries extends PageParameters {

    private List<InquiryDto> inquiryDtoList;

}
