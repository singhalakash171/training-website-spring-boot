package com.example.trainingwebsite.dto;

import lombok.Data;

@Data
public class PageParameters {

    Integer currentPage;

    Long totalItems;

    Integer totalPages;
}
