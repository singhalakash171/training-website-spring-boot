package com.example.trainingwebsite;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileReader;
import java.util.List;

public class Parser {

    public static void main(String[] args) throws Exception{

        Root[] data = new Gson().fromJson(new FileReader
                (new File("/Users/a.singhal/Documents/Yatharth/training-website-spring-boot/training-website/JsonData")),Root[].class);

        for(Root dataObject : data){
            System.out.println(dataObject.questionId);
        }
        for(Root dataObject : data){
            System.out.println(dataObject.resourceType);
        }

        for(Root dataObject : data){
            System.out.println(dataObject.curriculumOutcomes.get(0)._id);
        }
    }
}

class CurriculumOutcome{
    public String _id;
    public String type;
    public String name;
    public String description;
    public String curriculum;
    public String grade;
    public String subject;
}

class Root{
    public String questionId;
    public String resourceType;
    public List<CurriculumOutcome> curriculumOutcomes;
}
