package com.example.trainingwebsite.repository.training;

import com.example.trainingwebsite.model.training.TrainingModule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface TrainingModuleRepository extends JpaRepository<TrainingModule, Long> {

    TrainingModule findByIdAndActive(Integer moduleId, boolean active);

    List<TrainingModule> findAllByTrainingProgramIdOrderBySequenceAsc(int trainingId);

    List<TrainingModule> findAllByActive(boolean active);
}
