package com.example.trainingwebsite.repository.questions;

import com.example.trainingwebsite.model.questions.QuestionModule;
import com.example.trainingwebsite.model.training.TrainingModule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface QuestionModuleRepository extends JpaRepository<QuestionModule, Long> {

    QuestionModule findByIdAndActive(Integer moduleId, boolean active);

    List<QuestionModule> findAllByActive(boolean active);
}
