package com.example.trainingwebsite.repository.training;

import com.example.trainingwebsite.model.training.TrainingFaq;
import com.example.trainingwebsite.model.training.TrainingTopic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface TrainingFaqRepository extends JpaRepository<TrainingFaq, Long> {

    TrainingFaq findByIdAndActiveOrderBySequenceAsc(Integer trainingFaq,boolean active);
}
