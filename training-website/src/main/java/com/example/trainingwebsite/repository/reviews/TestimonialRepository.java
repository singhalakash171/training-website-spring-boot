package com.example.trainingwebsite.repository.reviews;

import com.example.trainingwebsite.model.reviews.Testimonial;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface TestimonialRepository extends JpaRepository<Testimonial, Long> {

    List<Testimonial> findAllByActive(boolean active);
    Testimonial findByIdAndActive(Integer id,boolean active);

}
