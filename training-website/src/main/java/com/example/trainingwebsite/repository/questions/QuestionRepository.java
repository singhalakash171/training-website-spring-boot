package com.example.trainingwebsite.repository.questions;

import com.example.trainingwebsite.config.ElasticIndex;
import com.example.trainingwebsite.model.questions.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.jpa.repository.JpaRepository;

@Document(indexName = ElasticIndex.QUESTIONS)
public interface QuestionRepository extends JpaRepository<Question, Long> {

    Page<Question> findByQuestionTextContainingAndActive(String searchText, Boolean active, Pageable pageable);
    Page<Question> findByQuestionTextContainingAndQuestionModuleIdAndActive(String searchText, Integer moduleId, Boolean active, Pageable pageable);
    Question findByIdAndActive(Integer id, boolean active);

}
