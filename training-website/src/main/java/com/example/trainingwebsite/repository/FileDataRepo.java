package com.example.trainingwebsite.repository;

import com.example.trainingwebsite.model.FileData;
import com.example.trainingwebsite.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Id;
import java.util.Optional;

@Repository
public interface FileDataRepo extends JpaRepository<FileData, Id> {

}
