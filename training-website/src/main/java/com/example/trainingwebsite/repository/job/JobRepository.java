package com.example.trainingwebsite.repository.job;

import com.example.trainingwebsite.model.placement.JobDetails;
import com.example.trainingwebsite.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface JobRepository extends JpaRepository<JobDetails, Long> {

    List<JobDetails> findAllByActiveAndStatus(boolean active, Status status);

    JobDetails findByActiveAndId(boolean active, Integer id);

}
