package com.example.trainingwebsite.repository.training;

import com.example.trainingwebsite.model.training.TrainingSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface TrainingScheduleRepository extends JpaRepository<TrainingSchedule, Long> {

    List<TrainingSchedule> findAllByActive(boolean active);

    //List<TrainingSchedule> findAllByActiveAndTrainingProgramId(boolean active, Integer trainingProgramId);
}
