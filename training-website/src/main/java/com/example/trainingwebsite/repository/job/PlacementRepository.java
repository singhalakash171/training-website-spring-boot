package com.example.trainingwebsite.repository.job;

import com.example.trainingwebsite.model.placement.JobPlacementDetails;
import com.example.trainingwebsite.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface PlacementRepository extends JpaRepository<JobPlacementDetails, Long> {

    List<JobPlacementDetails> findAllByActiveAndStatus(boolean active, Status status);

    JobPlacementDetails findByIdAndActive(Integer id, boolean active);

}
