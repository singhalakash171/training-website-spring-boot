package com.example.trainingwebsite.repository.inquiry;

import com.example.trainingwebsite.model.inquiry.InquiryAction;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InquiryActionRepo extends JpaRepository<InquiryAction, Long> {

    InquiryAction findByIdAndActive(Integer id, boolean active);
}
