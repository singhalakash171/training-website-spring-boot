package com.example.trainingwebsite.repository.training;

import com.example.trainingwebsite.model.training.TrainingTopic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface TrainingTopicRepository extends JpaRepository<TrainingTopic, Long> {
    List<TrainingTopic> findAllByTrainingModuleIdOrderBySequenceAsc(int trainingModuleId);

    TrainingTopic findByIdAndActive(Integer trainingId,boolean active);
}
