package com.example.trainingwebsite.repository.training;

import com.example.trainingwebsite.model.training.TrainingFaq;
import com.example.trainingwebsite.model.training.TrainingFeature;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TrainingFeaturesRepository extends JpaRepository<TrainingFeature, Long> {

    TrainingFeature findByIdAndActiveOrderBySequenceAsc(Integer featureId,boolean active);
}
