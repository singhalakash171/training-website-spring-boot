package com.example.trainingwebsite.repository.reviews;

import com.example.trainingwebsite.model.reviews.InternetReviews;
import com.example.trainingwebsite.model.training.TrainingModule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface InternetReviewsRepository extends JpaRepository<InternetReviews, Long> {

    List<InternetReviews> findAllByActive(boolean active);
}
