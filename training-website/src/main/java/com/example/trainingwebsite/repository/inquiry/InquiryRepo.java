package com.example.trainingwebsite.repository.inquiry;

import com.example.trainingwebsite.enums.InquiryStatus;
import com.example.trainingwebsite.model.inquiry.Inquiry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InquiryRepo extends JpaRepository<Inquiry, Long> {

    Inquiry findByIdAndActive(Integer id, boolean active);

    Page<Inquiry> findAllByActive(boolean active, Pageable pageable);

    Page<Inquiry> findAllByInquiryManagerIdAndActive(Integer leadManagerId, boolean active, Pageable pageable);
    Page<Inquiry> findAllByInquiryStatusAndActive(InquiryStatus inquiryStatus, boolean active, Pageable pageable);

    Page<Inquiry> findAllByInquiryStatusAndInquiryManagerIdAndActive(InquiryStatus inquiryStatus, Integer leadManagerId,
                                                                     boolean active, Pageable pageable);


}
