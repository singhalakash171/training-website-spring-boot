package com.example.trainingwebsite.repository.training;

import com.example.trainingwebsite.model.training.TrainingCategory;
import com.example.trainingwebsite.model.training.TrainingProgram;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface TrainingProgramRepository extends JpaRepository<TrainingProgram, Long> {

    TrainingProgram findByIdAndActive(int programId, boolean active);

    List<TrainingProgram> findAllByActiveOrderBySequenceAsc(boolean active);

    List<TrainingProgram> findAllByActiveAndTrainingCategory(boolean active, TrainingCategory trainingCategory);

}
