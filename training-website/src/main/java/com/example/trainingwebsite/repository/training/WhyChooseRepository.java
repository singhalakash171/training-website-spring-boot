package com.example.trainingwebsite.repository.training;

import com.example.trainingwebsite.model.training.TrainingWhyChoose;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface WhyChooseRepository extends JpaRepository<TrainingWhyChoose, Long> {

    TrainingWhyChoose findByIdAndActive(Integer id, boolean active);

    List<TrainingWhyChoose> findAllByTrainingProgramIdAndActiveOrderBySequenceAsc(Integer trainingProgramId, boolean active);
}
