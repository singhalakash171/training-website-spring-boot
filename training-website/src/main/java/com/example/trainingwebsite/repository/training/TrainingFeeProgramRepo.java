package com.example.trainingwebsite.repository.training;

import com.example.trainingwebsite.model.training.TrainingFeeProgram;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TrainingFeeProgramRepo extends JpaRepository<TrainingFeeProgram, Long> {

    TrainingFeeProgram findByIdAndActiveOrderBySequenceAsc(int programId, boolean active);

    TrainingFeeProgram findByIdAndActive(int programId, boolean active);

}
