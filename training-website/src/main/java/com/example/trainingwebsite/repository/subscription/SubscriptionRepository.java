package com.example.trainingwebsite.repository.subscription;

import com.example.trainingwebsite.model.questions.Question;
import com.example.trainingwebsite.model.subscription.SubscriptionDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface SubscriptionRepository extends JpaRepository<SubscriptionDetails, Long> {

    SubscriptionDetails findAllByActive(boolean active);

}
