package com.example.trainingwebsite.repository.user;

import java.util.List;
import java.util.Optional;

import com.example.trainingwebsite.enums.UserRole;
import com.example.trainingwebsite.model.user.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Id;

@Repository
public interface UserRepo extends JpaRepository<User, Id> {

    Optional<User> findByEmailIdAndActive(String emailId,boolean active);

    User findByIdAndActive(Integer id,boolean active);

    User findByIdAndUserRoleAndActive(Integer id, UserRole userRole,boolean active);

    User findByIdAndAndActive(Integer id,boolean active);

    List<User> findAllByActive(boolean active);

    User findByEmailIdAndActive(Integer id,boolean active);

    List<User> findAllByUserRoleAndActive(String userRole, boolean active);
}
