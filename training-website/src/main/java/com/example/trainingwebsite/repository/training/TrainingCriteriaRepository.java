package com.example.trainingwebsite.repository.training;

import com.example.trainingwebsite.model.training.TrainingCriteria;
import com.example.trainingwebsite.model.training.TrainingWhyChoose;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface TrainingCriteriaRepository extends JpaRepository<TrainingCriteria, Long> {

    TrainingCriteria findByIdAndActive(Integer id, boolean active);

    List<TrainingCriteria> findAllByTrainingProgramIdAndActiveOrderBySequenceAsc(Integer trainingProgramId, boolean active);
}
