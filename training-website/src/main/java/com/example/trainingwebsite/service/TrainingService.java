package com.example.trainingwebsite.service;

import com.example.trainingwebsite.dto.training.*;
import com.example.trainingwebsite.model.training.TrainingCategory;

import java.util.List;

public interface TrainingService {

    TrainingProgramDto createOrUpdateTrainingBasicDetails(TrainingProgramDto trainingProgramDto);

    TrainingProgramDto approveTraining(Integer trainingId);

    List<TrainingProgramDto> getAllTraining();

    List<TrainingProgramDto> getTrainingByCategory(TrainingCategory trainingCategory);

    TrainingModuleDto createOrUpdateModuleDetail(Integer trainingId, TrainingModuleDto lovDto);

    TrainingModuleDto approveModule(Integer moduleId);

    List<TrainingModuleDto> getModules(Integer programId,boolean includeTopics);

    List<TrainingFeeProgramDto> getTrainingFeeProgramDetails(Integer programId);

    TrainingTopicDto createOrUpdateTrainingTopic(Integer moduleId, TrainingTopicDto trainingTopicDto);

    TrainingTopicDto approveTopics(Integer topicId);

    TrainingFaqDto createOrUpdateTrainingFaq(Integer trainingId, TrainingFaqDto trainingFaqDto);

    TrainingFaqDto approveFaqQuestion(Integer trainingFaqId);

    TrainingFeatureDto createOrUpdateTrainingFeatures(Integer trainingId, TrainingFeatureDto trainingFeatureDto);

    TrainingFeatureDto approveFeature(Integer trainingFaqId);

    TrainingProgramDetailsDto getTrainingProgramDetails(Integer programId);

    //List<TrainingScheduleDto> getTrainingProgramSchedule(Integer programId);

    TrainingFeeProgramDto createTrainingFeeProgram(TrainingFeeProgramDto trainingFeeProgramDto, Integer trainingId);

    TrainingFeeProgramDto approveFeeProgram(Integer programId);

    List<TrainingFeatureDto> getTrainingFeatures(Integer trainingId);

    List<WhyChooseDto> getTrainingWhyChoose(Integer trainingId);

    WhyChooseDto createOrUpdateTrainingWhyChoose(Integer trainingId, WhyChooseDto whyChooseDto);

    WhyChooseDto approveTrainingWhyChoose(Integer whyChooseId);

    TrainingCriteriaDto createOrUpdateTrainingCriteria(Integer trainingId, TrainingCriteriaDto trainingCriteriaDto);

    TrainingCriteriaDto approveTrainingCriteria(Integer trainingCriteriaId);

    List<TrainingCriteriaDto> getAllTrainingCriteriaList(Integer trainingId);

    void linkTrainingProgramToTrainer(Integer trainingProgramId,Integer trainerId);

    List<TrainingFaqDto> getFaqDetails(Integer programId);
}
