package com.example.trainingwebsite.service;

import com.example.trainingwebsite.dto.inquiry.InquiryDto;
import com.example.trainingwebsite.dto.inquiry.InquiryUpdateDto;
import com.example.trainingwebsite.dto.inquiry.PageableInquiries;
import com.example.trainingwebsite.enums.InquiryStatus;
import com.example.trainingwebsite.enums.UserRole;
import com.example.trainingwebsite.model.user.User;
import org.springframework.data.domain.Pageable;

public interface InquiryService {

    PageableInquiries getAllInquiries(InquiryStatus inquiryStatus, Pageable pageable);

    InquiryDto getInquiryDetails(Integer inquiryId);

    InquiryDto createInquiry(InquiryDto inquiryDto);

    InquiryDto updateInquiry(InquiryDto inquiryDto);

    InquiryDto updateInquiryStatus(InquiryUpdateDto inquiryUpdateDto);

    void updateInquiryAssignedRoleRole(InquiryUpdateDto inquiryUpdateDto, UserRole userRole);

    void convertInquiryToCandidate(Integer leadId);

    User validateInquiryAllowedToUpdate(Integer leadId);
}
