package com.example.trainingwebsite.service;

import com.example.trainingwebsite.dto.questions.PageableQuestionDto;
import com.example.trainingwebsite.dto.questions.QuestionDto;
import com.example.trainingwebsite.dto.questions.QuestionModuleDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface QuestionService {

    QuestionModuleDto createOrUpdateQuestionModule(QuestionModuleDto questionModuleDto);

    List<QuestionModuleDto> getAllQuestionModules();

    PageableQuestionDto getQuestions(Integer moduleId,String searchText, Pageable pageable);

    QuestionDto createOrUpdateQuestion(QuestionDto questionDto);

    QuestionDto approveQuestion(Integer questionId);
}
