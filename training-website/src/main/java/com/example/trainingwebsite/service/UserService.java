package com.example.trainingwebsite.service;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.dto.users.*;
import com.example.trainingwebsite.model.user.User;

import java.util.List;

public interface UserService {

    UserDetailsDto registerUser(UserRegistrationDto userRegistrationDto);

    void changePasswordAsAdmin(UpdatePasswordDto updatePasswordDto);

    void changePasswordAsUser(UpdatePasswordDto updatePasswordDto);

    void lockOrUnlockUser(Integer userId,boolean updatedStatus);

    UserDetailsDto updateUserDetails(UserDetailsDto userDetailsDto);

    void updateUserRole(UpdateUserRoleDto updateUserRoleDto);

    UserDetailsDto getCurrentUserDetails();

    List<UserDetailsLovDto> getAllUsers();

    UserDetailsLovDto getUserDetailLovDto(Integer userId);
}
