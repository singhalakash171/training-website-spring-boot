package com.example.trainingwebsite.service;

import com.example.trainingwebsite.dto.reviews.TestimonialDto;
import com.example.trainingwebsite.model.reviews.InternetReviews;
import com.example.trainingwebsite.model.reviews.Testimonial;

import java.util.List;

public interface ReviewsService {

    List<InternetReviews> getLatestInternetReviews();

    List<Testimonial> getLatestTestimonials();

    TestimonialDto createOrUpdateTestimonial(TestimonialDto testimonial);

    TestimonialDto approveTestimonial(Integer testimonialId);

}
