package com.example.trainingwebsite.service;

import com.example.trainingwebsite.model.placement.JobDetails;
import com.example.trainingwebsite.model.placement.JobPlacementDetails;

import java.util.List;

public interface JobService {

    JobDetails addOrUpdateJobDetails(JobDetails jobDetails);

    List<JobDetails> getAllJobDetails();

    JobDetails approvePostedJob(Integer jobId);

    JobPlacementDetails approvePlacement(Integer placementId);
}
