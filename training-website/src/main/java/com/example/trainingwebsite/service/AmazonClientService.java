package com.example.trainingwebsite.service;

import com.example.trainingwebsite.enums.FileCategory;
import com.example.trainingwebsite.enums.FileSubCategory;
import org.springframework.web.multipart.MultipartFile;

public interface AmazonClientService {

    public String uploadFile(FileCategory fileCategory, FileSubCategory subCategory, Integer id, MultipartFile multipartFile);

}
