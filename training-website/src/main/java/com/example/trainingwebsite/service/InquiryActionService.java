package com.example.trainingwebsite.service;

import com.example.trainingwebsite.dto.inquiry.InquiryActionDetailsDto;
import com.example.trainingwebsite.model.inquiry.InquiryAction;

import java.util.List;

public interface InquiryActionService {

    InquiryActionDetailsDto createOrUpdateInquiryAction(InquiryActionDetailsDto inquiryActionDetailsDto);

    List<InquiryActionDetailsDto> toInquiryActionDetailsDtos(List<InquiryAction> inquiryActionList);
}
