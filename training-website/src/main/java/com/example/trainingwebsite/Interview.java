package com.example.trainingwebsite;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Interview {

    public static void main(String[] args) {
        System.out.println(updateString("Freedom Mortgage"));
    }

    public static String updateString(String str) {
        String updatedString = "";

        List<String> words = Arrays.asList(str.split(" "));
        for(String word : words){
            Set<Character> distinctCharters = new HashSet<>();
            for(int i=1;i<word.length()-1;i++){
                distinctCharters.add(word.charAt(i));
            }
            updatedString = updatedString + " " + word.charAt(0)+"" + distinctCharters.size()+"" + word.charAt(word.length()-1);
        }
        return updatedString;
    }
}
