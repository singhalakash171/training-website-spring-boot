package com.example.trainingwebsite.exception;

import java.util.*;
import java.util.stream.Collectors;

import com.example.trainingwebsite.dto.ErrorDetails;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = IncorrectStateException.class)
    public ResponseEntity<Object> exception(IncorrectStateException exception) {
        List<String> errorList = new ArrayList<>();
        errorList.add(exception.getMessage());
        return new ResponseEntity<>(new ErrorDetails(HttpStatus.BAD_REQUEST.value(),errorList), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        return new ResponseEntity<>(new ErrorDetails(HttpStatus.BAD_REQUEST.value(),errors), HttpStatus.BAD_REQUEST);
    }



}