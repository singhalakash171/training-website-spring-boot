package com.example.trainingwebsite.exception;

public class IncorrectStateException extends RuntimeException{

    public IncorrectStateException(String message){
        super(message);
    }
}
