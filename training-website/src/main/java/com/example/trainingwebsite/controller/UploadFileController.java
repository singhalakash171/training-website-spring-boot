package com.example.trainingwebsite.controller;

import com.example.trainingwebsite.enums.FileCategory;
import com.example.trainingwebsite.enums.FileSubCategory;
import com.example.trainingwebsite.service.AmazonClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/upload")
@CrossOrigin
public class UploadFileController {

    @Autowired
    AmazonClientService amazonClientService;

    @PostMapping(value = "/admin/uploadFile")
    public String uploadFile(@RequestParam(name = "category") FileCategory category,
                             @RequestParam(name = "subCategory") FileSubCategory subCategory,
                             @RequestParam(name = "id") Integer id,
                             @RequestParam(name = "file") MultipartFile file) {
        return amazonClientService.uploadFile(category,subCategory, id,file);
    }

}
