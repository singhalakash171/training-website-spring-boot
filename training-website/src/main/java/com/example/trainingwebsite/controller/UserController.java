package com.example.trainingwebsite.controller;

import com.example.trainingwebsite.config.security.MyUserSecurityService;
import com.example.trainingwebsite.config.security.AppUserDetails;
import com.example.trainingwebsite.config.security.dto.JwtRequest;
import com.example.trainingwebsite.config.security.dto.JwtResponse;
import com.example.trainingwebsite.config.security.jwt.util.JwtUtil;
import com.example.trainingwebsite.dto.users.*;
import com.example.trainingwebsite.enums.FileCategory;
import com.example.trainingwebsite.enums.FileSubCategory;
import com.example.trainingwebsite.model.user.User;
import com.example.trainingwebsite.service.AmazonClientService;
import com.example.trainingwebsite.service.UserService;
import com.example.trainingwebsite.serviceimpl.HelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

    @Autowired
    AmazonClientService amazonClientService;

    @Autowired
    HelperService helperService;

    @Autowired
    private UserService userService;

    @Autowired
	private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserSecurityService myUserSecurityService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/register")
    public ResponseEntity<UserDetailsDto> registerUser(@Valid @RequestBody UserRegistrationDto userRegistrationDto) {
        UserDetailsDto userDetailsDto = userService.registerUser(userRegistrationDto);
        return new ResponseEntity<>(userDetailsDto, HttpStatus.CREATED);
    }

    @PostMapping("/admin/updateRole")
    public ResponseEntity<String> updateUserRole(@Valid @RequestBody UpdateUserRoleDto updateUserRoleDto) {
        userService.updateUserRole(updateUserRoleDto);
        return new ResponseEntity<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

    @PostMapping("/admin/changePassword")
    public ResponseEntity<?> changePasswordAsAdmin(@RequestBody @Valid UpdatePasswordDto updatePasswordDto) {
        userService.changePasswordAsAdmin(updatePasswordDto);
        return new ResponseEntity<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

    @PostMapping("/changePassword")
    public ResponseEntity<?> changePasswordAsUser(@RequestBody UpdatePasswordDto updatePasswordDto) {
        userService.changePasswordAsUser(updatePasswordDto);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @GetMapping("/userDetails")
    public ResponseEntity<UserDetailsDto> getUserDetails() {
        return new ResponseEntity<>(userService.getCurrentUserDetails(), HttpStatus.OK);
    }

    @GetMapping("/{userId}/userDetails")
    public ResponseEntity<UserDetailsLovDto> getUserDetails(@PathVariable(name = "userId") Integer userId) {
        return new ResponseEntity<>(userService.getUserDetailLovDto(userId), HttpStatus.OK);
    }

    @PostMapping("/userDetails")
    public ResponseEntity<UserDetailsDto> updateUserDetails(@Valid @RequestBody UserDetailsDto userDetailsDto) {
        userDetailsDto = userService.updateUserDetails(userDetailsDto);
        return new ResponseEntity<>(userDetailsDto, HttpStatus.OK);
    }


    @PostMapping("/admin/lockUser/{userId}")
    public ResponseEntity<String> updateLockStatus(@PathVariable(name = "userId",required = true) Integer userId,
                                                   @RequestBody boolean updatedStatus) {
        userService.lockOrUnlockUser(userId,updatedStatus);
        return new ResponseEntity<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

    @PostMapping(value="/authenticate")
	public ResponseEntity<?> authenticate(@RequestBody @Valid JwtRequest jwtRequest) throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(), jwtRequest.getPassword()));
		} catch (BadCredentialsException b) {
			throw new Exception("Incorrect username Password");
		} catch (Exception e) {
			e.printStackTrace();
		}
		final AppUserDetails appUserDetails = myUserSecurityService.loadUserByUsername(jwtRequest.getUsername());
		final String jwt = jwtUtil.generateToken(appUserDetails);
		return new ResponseEntity<>(new JwtResponse(jwt), HttpStatus.OK);
	}

    @GetMapping("/admin/users")
    public ResponseEntity<?> getAllUsersList() {
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @PostMapping(value = "/profilePic")
    public ResponseEntity<?> uploadProfileImage(@RequestParam(name = "file") MultipartFile file) {
        User user = helperService.getUserFromSecurityContext();
        amazonClientService.uploadFile(FileCategory.USER, FileSubCategory.PROFILE_PIC,user.getId(),file);
        return new ResponseEntity<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

}
