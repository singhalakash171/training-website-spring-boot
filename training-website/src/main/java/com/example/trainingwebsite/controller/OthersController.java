package com.example.trainingwebsite.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class OthersController {


    @Autowired
    private JavaMailSender emailSender;

    @PostMapping("/sendEmail")
    public void sendSimpleMessage(@RequestParam(name = "to") String to,@RequestParam(name = "subject")  String subject,
                                  @RequestParam(name = "emailBody") String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@baeldung.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }
}
