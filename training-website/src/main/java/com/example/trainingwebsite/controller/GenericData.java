package com.example.trainingwebsite.controller;
import com.example.trainingwebsite.enums.*;
import com.example.trainingwebsite.model.inquiry.InquiryAction;
import com.example.trainingwebsite.model.training.TrainingCategory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/generic/")
@CrossOrigin
public class GenericData {

    @GetMapping("/status")
    public Status[] getStatus() {
        return Status.values();
    }

    @GetMapping("/difficultyLevel")
    public DifficultyLevel[] getDifficultyLevels() {
        return DifficultyLevel.values();
    }

    @GetMapping("/trainingCategories")
    public TrainingCategory[] getTrainingCategories() {
        return TrainingCategory.values();
    }

    @GetMapping("/roles")
    public UserRole[] getUserRoles() {
        return UserRole.values();
    }

    @GetMapping("/inquiryActions")
    public InquiryActionType[] getInquiryActions() {
        return InquiryActionType.values();
    }
    @GetMapping("/inquiryStatus")
    public InquiryStatus[] getInquiryStatus() {
        return InquiryStatus.values();
    }

}
