package com.example.trainingwebsite.controller;

import com.example.trainingwebsite.dto.inquiry.InquiryDto;
import com.example.trainingwebsite.dto.inquiry.InquiryActionDetailsDto;
import com.example.trainingwebsite.dto.inquiry.InquiryUpdateDto;
import com.example.trainingwebsite.dto.inquiry.PageableInquiries;
import com.example.trainingwebsite.enums.InquiryStatus;
import com.example.trainingwebsite.enums.UserRole;
import com.example.trainingwebsite.service.InquiryService;
import com.example.trainingwebsite.service.InquiryActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/inquiry")
@CrossOrigin
public class InquiryController {

    @Autowired
    InquiryService inquiryService;

    @Autowired
    InquiryActionService inquiryActionService;

    @GetMapping("/all")
    public PageableInquiries getAllInquiries(@RequestParam(required = false) InquiryStatus inquiryStatus,
                                             @RequestParam(name = "pageNo",defaultValue = "0") Integer pageNo,
                                             @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize,
                                             @RequestParam(name = "sortBys",defaultValue = "id") String sortBy,
                                             @RequestParam(name="dir",required = false) Sort.Direction direction)
    {
        if(direction == null)
            direction = Sort.Direction.ASC;
        Pageable pageable = PageRequest.of(pageNo,pageSize, Sort.by(direction,sortBy));
        return inquiryService.getAllInquiries(inquiryStatus,pageable);
    }

    @GetMapping("/details")
    public InquiryDto getInquiryDetails(@RequestParam(name = "leadId",required = true) Integer leadId) {
        return inquiryService.getInquiryDetails(leadId);
    }

    @PostMapping("/create")
    public InquiryDto createInquiry(@Valid @RequestBody InquiryDto inquiryDto) {
        return inquiryService.createInquiry(inquiryDto);
    }

    @PutMapping("/update")
    public InquiryDto updateInquiry(@Valid @RequestBody InquiryDto inquiryDto) {
        return inquiryService.updateInquiry(inquiryDto);
    }

    @PostMapping("/updateStatus")
    public InquiryDto updateInquiryStatus(@Valid @RequestBody InquiryUpdateDto inquiryUpdateDto) {
        return inquiryService.updateInquiryStatus(inquiryUpdateDto);
    }

    @PostMapping("/admin/assignLeadManager")
    public void assignInquiryManager(@RequestBody InquiryUpdateDto inquiryUpdateDto) {
        inquiryService.updateInquiryAssignedRoleRole(inquiryUpdateDto, UserRole.LEAD_MANAGER);
    }

    @PostMapping("/admin/convertInquiryToStudent")
    public void convertInquiryToStudent(@RequestBody Integer leadId) {
        inquiryService.convertInquiryToCandidate(leadId);
    }


    @PostMapping("/addInquiryActionDetails")
    public void addOrUpdateInquiryAction(@RequestBody InquiryActionDetailsDto inquiryActionDetailsDto) {
        inquiryActionService.createOrUpdateInquiryAction(inquiryActionDetailsDto);
    }

}
