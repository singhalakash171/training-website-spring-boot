package com.example.trainingwebsite.controller;

import com.example.trainingwebsite.dto.reviews.TestimonialDto;
import com.example.trainingwebsite.model.reviews.InternetReviews;
import com.example.trainingwebsite.model.reviews.Testimonial;
import com.example.trainingwebsite.service.ReviewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/reviews")
@CrossOrigin
public class ReviewsController {

    @Autowired
    private ReviewsService reviewsService;

    @GetMapping("/internet")
    public List<InternetReviews> getLatestInternetReviews() {
        return reviewsService.getLatestInternetReviews();
    }

    @GetMapping("/testimonial")
    public List<Testimonial> getLatestTestimonials() {
        return reviewsService.getLatestTestimonials();
    }

    @PostMapping("/testimonial/createOrUpdate")
    public TestimonialDto createOrUpdateTestimonial(@RequestBody(required = true) TestimonialDto testimonialDto) {
        return reviewsService.createOrUpdateTestimonial(testimonialDto);
    }

    @PutMapping("/testimonial/approve")
    public TestimonialDto approveTestimonial(@RequestParam(required = true,name = "id") Integer testimonialId) {
        return reviewsService.approveTestimonial(testimonialId);
    }
}
