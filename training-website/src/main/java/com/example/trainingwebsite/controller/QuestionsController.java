package com.example.trainingwebsite.controller;

import com.example.trainingwebsite.dto.questions.PageableQuestionDto;
import com.example.trainingwebsite.dto.questions.QuestionDto;
import com.example.trainingwebsite.dto.questions.QuestionModuleDto;
import com.example.trainingwebsite.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/question")
public class QuestionsController {

    @Autowired
    private QuestionService questionService;

    @PostMapping("/admin/module/createOrUpdateModule")
    public QuestionModuleDto createOrUpdatedQuestionModule(@RequestBody @Valid QuestionModuleDto questionModuleDto) {
        return questionService.createOrUpdateQuestionModule(questionModuleDto);
    }

    @GetMapping("/module/all")
    public List<QuestionModuleDto> getAllQuestionModules() {
        return questionService.getAllQuestionModules();
    }

    @GetMapping("/all")
    public PageableQuestionDto getQuestions(@RequestParam(name = "moduleId", required = false) Integer moduleId,
                                            @RequestParam(name = "searchText",defaultValue = "") String searchText,
                                            @RequestParam(name = "pageNo",defaultValue = "0") Integer pageNo,
                                            @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize,
                                            @RequestParam(name = "sortBys",defaultValue = "id") String sortBy,
                                            @RequestParam(name="dir",required = false) Sort.Direction direction) {

        if(direction == null)
            direction = Sort.Direction.ASC;
        Pageable pageable = PageRequest.of(pageNo,pageSize, Sort.by(direction,sortBy));
        return questionService.getQuestions(moduleId,searchText,pageable);
    }

    @PostMapping("/admin/createOrUpdate")
    public QuestionDto createOrUpdateQuestion(@RequestBody @Valid QuestionDto questionDto) {
        return questionService.createOrUpdateQuestion(questionDto);
    }

    @PostMapping("/approve")
    public QuestionDto approveQuestion(@RequestParam(name = "id") Integer questionId) {
        return questionService.approveQuestion(questionId);
    }

}
