package com.example.trainingwebsite.controller;

import com.example.trainingwebsite.model.placement.JobDetails;
import com.example.trainingwebsite.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/job/")
@CrossOrigin
public class JobController {

    @Autowired
    private JobService jobService;

    @GetMapping("/openings")
    public List<JobDetails> getLatestOpenings() {
        return jobService.getAllJobDetails();
    }

    @PostMapping("/createOrUpdate")
    public JobDetails createOrUpdateOpening(@RequestBody(required = true) JobDetails jobDetails) {
        return jobService.addOrUpdateJobDetails(jobDetails);
    }

    @PostMapping("/approve")
    public JobDetails approveJob(@RequestParam(required = true) Integer jobId) {
        return jobService.approvePostedJob(jobId);
    }
}
