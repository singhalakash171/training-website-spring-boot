package com.example.trainingwebsite.controller;

import com.example.trainingwebsite.dto.training.*;
import com.example.trainingwebsite.model.training.TrainingCategory;
import com.example.trainingwebsite.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/training")
@CrossOrigin
public class TrainingController {

    @Autowired
    private TrainingService trainingService;

    @PostMapping("/admin/create")
    public TrainingProgramDto createOrUpdateTraining(@Valid @RequestBody TrainingProgramDto trainingProgramDto) {
        return trainingService.createOrUpdateTrainingBasicDetails(trainingProgramDto);
    }

    @PutMapping("/admin/approve/{trainingId}")
    public void approveTraining(@PathVariable(name = "trainingId" , required = true) Integer trainingId) {
        trainingService.approveTraining(trainingId);
    }

    @GetMapping("/trainings")
    public List<TrainingProgramDto> getAllTrainings(@RequestParam(name = "category", required = false) String trainingCategory) {
        if (StringUtils.isEmpty(trainingCategory)) {
            return trainingService.getAllTraining();
        } else {
            return trainingService.getTrainingByCategory(TrainingCategory.valueOf(trainingCategory.toUpperCase()));
        }
    }

    @GetMapping("/details/{trainingId}")
    public TrainingProgramDetailsDto getTrainingDetails(@PathVariable(name = "trainingId", required = true) Integer trainingId) {
        return trainingService.getTrainingProgramDetails(trainingId);
    }

    @PostMapping("/admin/module")
    public TrainingModuleDto createOrUpdateTrainingTopic(@Valid @RequestBody TrainingModuleDto trainingModuleDto, @RequestParam(required = true,name = "trainingId") Integer trainingId) {
        return trainingService.createOrUpdateModuleDetail(trainingId,trainingModuleDto);
    }

    @PutMapping("/admin/module/approve/{moduleId}")
    public void approveModule(@PathVariable(name = "moduleId" , required = true) Integer moduleId) {
        trainingService.approveModule(moduleId);
    }

    @GetMapping("/module/moduleDetails")
    public List<TrainingModuleDto> getModulesList(@RequestParam(name = "programId",required = false) Integer programId,@RequestParam(name = "includeTopics",required = false) boolean includeTopics) {
        System.out.println("Training modules");
        return trainingService.getModules(programId,includeTopics);
    }

    @PostMapping("/admin/topic")
    public TrainingTopicDto createOrUpdateTrainingTopic(@Valid @RequestBody TrainingTopicDto trainingTopicDto, @RequestParam(required = true,name = "moduleId") Integer moduleId) {
        return trainingService.createOrUpdateTrainingTopic(moduleId,trainingTopicDto);
    }

    @PutMapping("/admin/topic/approve/{topicId}")
    public void approveTopic(@PathVariable(name = "topicId" , required = true) Integer topicId) {
        trainingService.approveTopics(topicId);
    }

    @PostMapping("/admin/faq")
    public TrainingFaqDto createOrUpdateFaq(@Valid @RequestBody TrainingFaqDto trainingFaqDto, @RequestParam(required = true) Integer trainingId) {
        return trainingService.createOrUpdateTrainingFaq(trainingId,trainingFaqDto);
    }

    @GetMapping("/faq")
    public List<TrainingFaqDto> getTrainingFaq(@RequestParam(name = "trainingId" , required = true) Integer trainingId) {
        return trainingService.getFaqDetails(trainingId);
    }

    @PutMapping("/admin/faq/approve/{faqId}")
    public void approveFaq(@PathVariable(name = "faqId" , required = true) Integer faqId) {
        trainingService.approveFaqQuestion(faqId);
    }

    @GetMapping("/feature")
    public List<TrainingFeatureDto> getTrainingFeatures(@RequestParam(name = "trainingId",required = true) Integer trainingId) {
        return trainingService.getTrainingFeatures(trainingId);
    }

    @PostMapping("/admin/feature")
    public TrainingFeatureDto createOrUpdateFeatures(@Valid @RequestBody TrainingFeatureDto trainingFeatureDto,@RequestParam(required = true) Integer trainingId) {
        return trainingService.createOrUpdateTrainingFeatures(trainingId,trainingFeatureDto);
    }

    @PutMapping("/admin/feature/approve/{featureId}")
    public void approveFeatures(@PathVariable(name = "featureId" , required = true) Integer featureId) {
        trainingService.approveFeature(featureId);
    }

    @GetMapping("/whychoose")
    public List<WhyChooseDto> getWhyChooseProgram(@RequestParam(required = true) Integer trainingId) {
        return trainingService.getTrainingWhyChoose(trainingId);
    }

    @PostMapping("/admin/whychoose")
    public WhyChooseDto createOrUpdateWhyChoose(@Valid @RequestBody WhyChooseDto whyChooseDto,@RequestParam(required = true) Integer trainingId) {
        return trainingService.createOrUpdateTrainingWhyChoose(trainingId,whyChooseDto);
    }

    @PutMapping("/admin/whychoose/approve/{whyChooseId}")
    public void approveWhyChoose(@PathVariable(name = "whyChooseID" , required = true) Integer whyChooseId) {
        trainingService.approveTrainingWhyChoose(whyChooseId);
    }

    @GetMapping("/trainingCriteria")
    public List<TrainingCriteriaDto> getTrainingCriteriaList(@RequestParam(required = true) Integer trainingId) {
        return trainingService.getAllTrainingCriteriaList(trainingId);
    }

    @PostMapping("/admin/trainingCriteria")
    public TrainingCriteriaDto createOrUpdateTrainingCriteria(@Valid @RequestBody TrainingCriteriaDto trainingCriteriaDto,@RequestParam(required = true) Integer trainingId) {
        return trainingService.createOrUpdateTrainingCriteria(trainingId,trainingCriteriaDto);
    }

    @PutMapping("/admin/trainingCriteria/approve/{trainingCriteriaId}")
    public void approveTrainingCriteria(@PathVariable(name = "trainingCriteriaId" , required = true) Integer trainingCriteriaId) {
        trainingService.approveTrainingCriteria(trainingCriteriaId);
    }

    @GetMapping("/feeProgram")
    public List<TrainingFeeProgramDto> getFeeProgram(@RequestParam(required = true, name="trainingId") Integer trainingId) {
        return trainingService.getTrainingFeeProgramDetails(trainingId);
    }

    @PostMapping("/admin/feeProgram")
    public TrainingFeeProgramDto createFeeProgram(@Valid @RequestBody TrainingFeeProgramDto trainingFeeProgramDto, @RequestParam(required = true, name="trainingId") Integer trainingId) {
        return trainingService.createTrainingFeeProgram(trainingFeeProgramDto,trainingId);
    }

    @PutMapping("/admin/feeProgram/approve/{programId}")
    public void approveFeeProgram(@PathVariable(name = "programId" , required = true) Integer programId) {
        trainingService.approveFeeProgram(programId);
    }

    @PostMapping("/admin/linkTrainer")
    public void linkTrainingToTraining(@RequestParam(name = "trainingId" , required = true) Integer trainingId,
                                       @RequestParam(name = "userId" , required = true) Integer userId) {
        trainingService.linkTrainingProgramToTrainer(trainingId,userId);
    }


//    @GetMapping("/schedule")
//    public List<TrainingScheduleDto> getTrainingSchedule(@RequestParam(name = "id", required = true) Integer trainingId) {
//        return trainingService.getTrainingProgramSchedule(trainingId);
//    }
}
