package com.example.trainingwebsite.config.security;

import com.example.trainingwebsite.config.security.filter.JwtAuthenticationFilter;

import com.example.trainingwebsite.enums.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyUserSecurityService myUserSecurityService;

    @Autowired
    private JwtAuthenticationFilter jwtRequestFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserSecurityService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
			.authorizeRequests().antMatchers("/api/user/authenticate","/api/user/register",
						"/api/generic/**", "/api/reviews/**","/api/jobs/**","/api/lead/create","/api/user/profilePic","/api/user/{userId}/userDetails",
						"/api/question/all","/api/training/**")
				.permitAll()
				.antMatchers(HttpMethod.POST,"/api/user/admin/**").hasRole(UserRole.ADMIN.name())
				.antMatchers(HttpMethod.GET,"/api/user/admin/**").hasRole(UserRole.ADMIN.name())

				.antMatchers(HttpMethod.POST,"/api/training/admin/**").hasRole(UserRole.ADMIN.name())
				.antMatchers(HttpMethod.GET,"/api/training/admin/**").hasRole(UserRole.ADMIN.name())

			.antMatchers( HttpMethod.POST,"/api/question/admin/**").hasRole(UserRole.ADMIN.name())
				.antMatchers( HttpMethod.GET,"/api/question/admin/**").hasRole(UserRole.ADMIN.name())

			.antMatchers( HttpMethod.POST,"/api/lead/admin/**").hasRole(UserRole.ADMIN.name())
				.antMatchers( HttpMethod.GET,"/api/lead/admin/**").hasRole(UserRole.ADMIN.name())

			.antMatchers( HttpMethod.POST,"/api/upload/admin/**").hasRole(UserRole.ADMIN.name())
				.antMatchers( HttpMethod.GET,"/api/upload/admin/**").hasRole(UserRole.ADMIN.name())

				.antMatchers( HttpMethod.PUT,"/api/lead/update").hasRole(UserRole.ADMIN.name())
				.antMatchers( HttpMethod.PUT,"/api/lead/update").hasRole(UserRole.LEAD_MANAGER.name())

			.and().sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
	public PasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
}
