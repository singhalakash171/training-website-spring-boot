package com.example.trainingwebsite.config.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
public class AppUserDetails implements UserDetails {

    private String username;

    private String password;

    private List<SimpleGrantedAuthority> grantedAuthorities;

	private boolean isAccountNonExpired;

	private boolean isEnabled;

	private boolean isCredentialNonExpired;

	private boolean isAccountNonLocked;

    private boolean isPasswordUpdateRequired;

	private String consumerName;

    public AppUserDetails(String emailId, String password,String grantedAuthorities,
            boolean isAccountNonExpired, boolean isEnabled, boolean isCredentialNonExpired,
            boolean isAccountNonLocked,boolean isPasswordUpdateRequired) {

        this.grantedAuthorities = Arrays.asList(grantedAuthorities.split(";")).stream().map(
                grantedAuthority -> new SimpleGrantedAuthority("ROLE_" +
                        grantedAuthority)).collect(Collectors.toList());

        this.username = emailId;
        this.password = password;
        this.isAccountNonExpired = isAccountNonExpired;
        this.isEnabled = isEnabled;
        this.isCredentialNonExpired = isCredentialNonExpired;
        this.isAccountNonLocked = isAccountNonLocked;
        this.isPasswordUpdateRequired = isPasswordUpdateRequired;
        
    }

    public Collection<? extends GrantedAuthority> getAuthorities(){
        return grantedAuthorities;
    }

    public boolean isAccountNonExpired(){
        return this.isAccountNonExpired;
    }

    public boolean isAccountNonLocked(){
        return this.isAccountNonLocked;
    }

    public boolean isCredentialsNonExpired(){
        return this.isCredentialNonExpired;
    }

    public boolean isEnabled(){
        return this.isEnabled;
    }
}
