package com.example.trainingwebsite.config.security;

import java.util.Optional;

import com.example.trainingwebsite.model.user.User;
import com.example.trainingwebsite.repository.user.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserSecurityService implements UserDetailsService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public AppUserDetails loadUserByUsername(String userEmail) throws UsernameNotFoundException { 
        Optional<User> optionalUser = userRepo.findByEmailIdAndActive(userEmail,true);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            return new AppUserDetails(user.getEmailId(), user.getPassword(),user.getUserRole().name(),
                        user.isAccountNonExpired(),
                        user.isActive(),
                        user.isCredentialsNonExpired(),
                        user.isAccountNonLocked(),user.isUpdatePasswordRequired());
        }
        return null;
    }
    
}
