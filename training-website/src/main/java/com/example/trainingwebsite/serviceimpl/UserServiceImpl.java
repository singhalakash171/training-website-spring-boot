package com.example.trainingwebsite.serviceimpl;

import com.amazonaws.util.StringUtils;
import com.example.trainingwebsite.dto.users.*;
import com.example.trainingwebsite.enums.UserRole;
import com.example.trainingwebsite.exception.IncorrectStateException;
import com.example.trainingwebsite.model.user.User;
import com.example.trainingwebsite.model.user.UserDetails;
import com.example.trainingwebsite.repository.user.UserRepo;
import com.example.trainingwebsite.service.UserService;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
    private UserRepo userRepo;

	@Autowired
	private HelperService helperService;

    @Override
    public UserDetailsDto registerUser(UserRegistrationDto userRegistrationDto) {
		validateRegisterRequest(userRegistrationDto);
        User user = toUserEntity(userRegistrationDto);
		user =  userRepo.save(user);
		return toUserDetailsDto(user);
    }

	public void updateUserRole(UpdateUserRoleDto updateUserRoleDto){

		if(updateUserRoleDto.getUserRole() == null)
			throw new IncorrectStateException("UserRole is required");

		User user = userRepo.findByIdAndActive(updateUserRoleDto.getUserId(),true);
		if(user == null)
			throw new IncorrectStateException("User does not exists having id : " + updateUserRoleDto.getUserId());

		user.setUserRole(updateUserRoleDto.getUserRole());
		userRepo.save(user);
	}

    private User toUserEntity(UserRegistrationDto userRegistrationDto) {
		User user = new User();
		user.setFirstName(userRegistrationDto.getFirstName());
		user.setLastName(userRegistrationDto.getLastName());
		user.setEmailId(userRegistrationDto.getEmailId().toLowerCase());
		user.setUserRole(UserRole.USER);
		user.setPassword(new BCryptPasswordEncoder().encode(userRegistrationDto.getPassword()));
		user.setAccountNonExpired(true);
		user.setAccountNonLocked(true);
		user.setCredentialsNonExpired(true);
		user.setVerifiedAccount(false);
		user.setAdminRegistered(false);
		user.setActive(true);
		UserDetails userDetails = new UserDetails();
		userDetails.setPhoneNumber(userRegistrationDto.getPhoneNumber());
		userDetails.setFirstName(userRegistrationDto.getFirstName());
		userDetails.setLastName(userRegistrationDto.getLastName());
		userDetails.setPhoneVerified(false);
		user.setUserDetails(userDetails);
		return user;
	}

	public UserDetailsDto updateUserDetails(UserDetailsDto userDetailsDto){
		User user = userRepo.findByIdAndActive(userDetailsDto.getId(),true);
		user.setFirstName(userDetailsDto.getFirstName());
		user.setLastName(userDetailsDto.getLastName());
		UserDetails userDetails = user.getUserDetails();
		userDetails.setFirstName(userDetailsDto.getFirstName());
		userDetails.setMiddleName(userDetailsDto.getMiddleName());
		userDetails.setLastName(userDetailsDto.getLastName());
		userDetails.setAddressLine1(userDetailsDto.getAddressLine1());
		userDetails.setAddressLine2(userDetailsDto.getAddressLine2());
		userDetails.setPinCode(userDetailsDto.getPinCode());
		user =  userRepo.save(user);
		return toUserDetailsDto(user);
	}

	public List<UserDetailsLovDto> getAllUsers(){
		List<User> users = userRepo.findAllByActive(true);
		return toUserDetailsLovDtos(users);
	}

	public UserDetailsDto getCurrentUserDetails(){
		User user = helperService.getUserFromSecurityContext();
		return toUserDetailsDto(user);
	}

	public UserDetailsLovDto getUserDetailLovDto(Integer userId){
		User user = helperService.getActiveUserById(userId);
		UserDetailsLovDto userDetailsLovDto = new UserDetailsLovDto();
		return toUserDetailsLovDto(userDetailsLovDto,user);
	}

	public UserDetailsDto toUserDetailsDto(User user){
		UserDetailsDto userDetailsDto = new UserDetailsDto();
		toUserDetailsLovDto(userDetailsDto,user);
		userDetailsDto.setPinCode(user.getUserDetails().getPinCode());
		userDetailsDto.setProfilePic(user.getProfilePic());
		userDetailsDto.setAddressLine1(user.getUserDetails().getAddressLine1());
		userDetailsDto.setAddressLine2(user.getUserDetails().getAddressLine2());
		return userDetailsDto;
	}

	public UserDetailsLovDto toUserDetailsLovDto(UserDetailsLovDto userDetailsLovDto,User user){
		userDetailsLovDto.setId(user.getId());
		userDetailsLovDto.setFirstName(user.getFirstName());
		userDetailsLovDto.setMiddleName(user.getMiddleName());
		userDetailsLovDto.setLastName(user.getLastName());
		userDetailsLovDto.setEmailId(user.getEmailId());
		userDetailsLovDto.setVerifiedAccount(user.isVerifiedAccount());
		userDetailsLovDto.setAdminRegistered(user.isAdminRegistered());
		userDetailsLovDto.setPhoneNumber(user.getUserDetails().getPhoneNumber());
		return userDetailsLovDto;
	}

	public List<UserDetailsLovDto> toUserDetailsLovDtos(List<User> users){
		List<UserDetailsLovDto> userDetailsLovDtos = new ArrayList<>();
		if(!CollectionUtils.isEmpty(users)){
			users.forEach(e->{
				userDetailsLovDtos.add(toUserDetailsLovDto(new UserDetailsLovDto(),e));
			});
		}
		return userDetailsLovDtos;
	}

	public void lockOrUnlockUser(Integer userId,boolean updatedStatus){
		User user = userRepo.findByIdAndActive(userId,true);
		if(user == null)
			throw new IncorrectStateException("User does not exists having id : " + userId);
		user.setAccountNonLocked(updatedStatus);
		userRepo.save(user);
	}

	public void changePasswordAsAdmin(UpdatePasswordDto updatePasswordDto){
		User user = helperService.getActiveUserById(updatePasswordDto.getUserId());
		BCryptPasswordEncoder bCryptPasswordEncoder =  new BCryptPasswordEncoder();
		if(updatePasswordDto.getConfirmPassword().equals(updatePasswordDto.getNewPassword())){
			user.setPassword(bCryptPasswordEncoder.encode(updatePasswordDto.getConfirmPassword()));
		}else{
			throw new IncorrectStateException("Password and Confirm Passwords are not same");
		}
		userRepo.save(user);
	}

	public void changePasswordAsUser(UpdatePasswordDto updatePasswordDto){
		User user = helperService.getUserFromSecurityContext();
		BCryptPasswordEncoder bCryptPasswordEncoder =  new BCryptPasswordEncoder();

		if(StringUtils.isNullOrEmpty(updatePasswordDto.getExistingPassword()))
			throw new IncorrectStateException("Existing password can't be null/empty");

		if(bCryptPasswordEncoder.matches(updatePasswordDto.getExistingPassword(),user.getPassword())){
			if(updatePasswordDto.getConfirmPassword().equals(updatePasswordDto.getNewPassword())){
				user.setPassword(bCryptPasswordEncoder.encode(updatePasswordDto.getConfirmPassword()));
			}else{
				throw new IncorrectStateException("Password and Confirm Passwords are not same");
			}
		}else{
			throw new IncorrectStateException("Incorrect existing password");
		}
		userRepo.save(user);
	}

	public void validateRegisterRequest(UserRegistrationDto userRegistrationDto){
		Optional<User> optionalUser = userRepo.findByEmailIdAndActive(userRegistrationDto.getEmailId().toLowerCase(),true);
		if(optionalUser.isPresent())
			throw new IncorrectStateException("User having same emailId already registered,try login");
	}
}
