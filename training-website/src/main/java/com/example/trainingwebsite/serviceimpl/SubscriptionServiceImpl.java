package com.example.trainingwebsite.serviceimpl;


import com.example.trainingwebsite.repository.questions.QuestionRepository;
import com.example.trainingwebsite.repository.training.TrainingModuleRepository;
import com.example.trainingwebsite.service.SubscriptionService;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    QuestionRepository questionRepository;
    TrainingModuleRepository trainingModuleRepository;

    public SubscriptionServiceImpl(QuestionRepository questionRepository, TrainingModuleRepository trainingModuleRepository) {
        this.questionRepository = questionRepository;
        this.trainingModuleRepository = trainingModuleRepository;
    }


}
