package com.example.trainingwebsite.serviceimpl;
import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.dto.inquiry.InquiryActionDetailsDto;
import com.example.trainingwebsite.dto.inquiry.InquiryDto;
import com.example.trainingwebsite.dto.inquiry.InquiryUpdateDto;
import com.example.trainingwebsite.dto.inquiry.PageableInquiries;
import com.example.trainingwebsite.dto.users.UserRegistrationDto;
import com.example.trainingwebsite.enums.InquiryActionType;
import com.example.trainingwebsite.enums.InquiryStatus;
import com.example.trainingwebsite.enums.UserRole;
import com.example.trainingwebsite.exception.IncorrectStateException;
import com.example.trainingwebsite.model.inquiry.Inquiry;
import com.example.trainingwebsite.model.user.User;
import com.example.trainingwebsite.repository.inquiry.InquiryRepo;
import com.example.trainingwebsite.repository.user.UserRepo;
import com.example.trainingwebsite.service.InquiryActionService;
import com.example.trainingwebsite.service.InquiryService;
import com.example.trainingwebsite.service.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InquiryServiceImpl implements InquiryService {

    @Autowired
    HelperService helperService;

    @Autowired
    UserRepo userRepo;

    @Autowired
    UserService userService;

    @Autowired
    InquiryRepo inquiryRepo;

    @Autowired
    InquiryActionService inquiryActionService;


    public PageableInquiries getAllInquiries(InquiryStatus inquiryStatus, Pageable pageable){

        User user = helperService.getUserFromSecurityContext();
        PageableInquiries pageableInquiries = new PageableInquiries();
        Page<Inquiry> inquiries;

        if(inquiryStatus == null){
            if(user.getUserRole() == UserRole.ADMIN)
                inquiries = inquiryRepo.findAllByActive(true,pageable);
            else
                inquiries = inquiryRepo.findAllByInquiryManagerIdAndActive(user.getId(),true,pageable);
        }else{
            if(user.getUserRole() == UserRole.ADMIN)
                inquiries = inquiryRepo.findAllByInquiryStatusAndActive(inquiryStatus,true,pageable);
            else
                inquiries = inquiryRepo.findAllByInquiryStatusAndInquiryManagerIdAndActive(inquiryStatus,user.getId(),true,pageable);
        }

        pageableInquiries.setInquiryDtoList(toInquiryDtos(inquiries.toList()));
        pageableInquiries.setCurrentPage(inquiries.getNumber());
        pageableInquiries.setTotalPages(inquiries.getTotalPages());
        pageableInquiries.setTotalItems(inquiries.getTotalElements());

        return pageableInquiries;
    }

    public InquiryDto getInquiryDetails(Integer inquiryId){
        validateInquiryAllowedToUpdate(inquiryId);
        Inquiry inquiry = inquiryRepo.findByIdAndActive(inquiryId,true);
        InquiryDto inquiryDto = toInquiryDto(inquiry);
        inquiryDto.setInquiryActionDetailsDtoList(inquiryActionService.toInquiryActionDetailsDtos(inquiry.getInquiryActions()));
        return inquiryDto;
    }

    public InquiryDto createInquiry(InquiryDto inquiryDto){
        Inquiry inquiry = new Inquiry();
        inquiry.setInquiryStatus(InquiryStatus.NEW);
        toInquiryEntity(inquiry, inquiryDto);
        inquiry = inquiryRepo.save(inquiry);
        return toInquiryDto(inquiry);
    }

    public InquiryDto updateInquiry(InquiryDto inquiryDto){

        if(inquiryDto.getInquiryId()!=null){
            Inquiry inquiry = inquiryRepo.findByIdAndActive(inquiryDto.getInquiryId(),true);
            toInquiryEntity(inquiry, inquiryDto);
            inquiry = inquiryRepo.save(inquiry);
            return toInquiryDto(inquiry);
        }else
            throw new IncorrectStateException("InquiryId is required");

    }

    public InquiryDto updateInquiryStatus(InquiryUpdateDto inquiryUpdateDto){

        if(inquiryUpdateDto.getInquiryStatus()==null)
            throw new IncorrectStateException("Inquiry Status is required");

        Inquiry inquiry = inquiryRepo.findByIdAndActive(inquiryUpdateDto.getId(),true);
        if(inquiry == null)
            throw new IncorrectStateException("No inquiry found having id :" + inquiryUpdateDto.getId());

        inquiry.setInquiryStatus(inquiryUpdateDto.getInquiryStatus());
        inquiry = inquiryRepo.save(inquiry);
        InquiryActionDetailsDto inquiryActionDetailsDto = new InquiryActionDetailsDto();
        inquiryActionDetailsDto.setInquiryDto(new LovDto(inquiry.getId(),"",""));
        inquiryActionDetailsDto.setComments("System Generated");
        inquiryActionDetailsDto.setInquiryActionType(InquiryActionType.UPDATE_STATUS);
        inquiryActionService.createOrUpdateInquiryAction(inquiryActionDetailsDto);
        return toInquiryDto(inquiry);
    }

    public void updateInquiryAssignedRoleRole(InquiryUpdateDto inquiryUpdateDto, UserRole userRole){

        if(inquiryUpdateDto.getInquiryManagerId() == null)
            throw new IncorrectStateException("InquiryManagerId is required");

        Inquiry inquiry = inquiryRepo.findByIdAndActive(inquiryUpdateDto.getId(),true);
        if(inquiry !=null){
            User user = helperService.getActiveUserByRole(inquiryUpdateDto.getInquiryManagerId(),userRole);
            inquiry.setInquiryManager(user);
            inquiryRepo.save(inquiry);
        }else
            throw new IncorrectStateException("No inquiry found corresponding to InquiryId : " + inquiryUpdateDto.getId());

        inquiryRepo.save(inquiry);
    }

    public void convertInquiryToCandidate(Integer inquiryId){
        Inquiry inquiry = inquiryRepo.findByIdAndActive(inquiryId,true);
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setFirstName(inquiry.getFirstName());
        userRegistrationDto.setLastName(inquiry.getLastName());
        userRegistrationDto.setMiddleName(inquiry.getMiddleName());
        userRegistrationDto.setEmailId(inquiry.getEmail());
        userRegistrationDto.setPhoneNumber(inquiry.getPhoneNumber());
        userRegistrationDto.setPassword("user12345");
        userService.registerUser(userRegistrationDto);
    }

    public Inquiry toInquiryEntity(Inquiry inquiry, InquiryDto inquiryDto){
        inquiry.setActive(true);
        inquiry.setFirstName(inquiryDto.getFirstName());
        inquiry.setMiddleName(inquiryDto.getMiddleName());
        inquiry.setLastName(inquiryDto.getLastName());
        inquiry.setEmail(inquiryDto.getEmailId());
        inquiry.setPhoneNumber(inquiryDto.getPhoneNumber());
        inquiry.setInterestedProgram(inquiryDto.getInterestedProgram());
        inquiry.setComments(inquiryDto.getComments());
        return inquiry;
    }

    public List<InquiryDto> toInquiryDtos(List<Inquiry> inquiries){
        List<InquiryDto> inquiryDtos = new ArrayList<>();
        if(!CollectionUtils.isEmpty(inquiries)){
            inquiries.forEach(e->{
                inquiryDtos.add(toInquiryDto(e));
            });
        }
        return inquiryDtos;
    }

    public InquiryDto toInquiryDto(Inquiry inquiry){
        InquiryDto inquiryDto = new InquiryDto();
        inquiryDto.setInquiryId(inquiry.getId());
        inquiryDto.setFirstName(inquiry.getFirstName());
        inquiryDto.setMiddleName(inquiry.getMiddleName());
        inquiryDto.setLastName(inquiry.getLastName());
        inquiryDto.setEmailId(inquiry.getEmail());
        inquiryDto.setPhoneNumber(inquiry.getPhoneNumber());
        inquiryDto.setComments(inquiry.getComments());
        inquiryDto.setInquiryStatus(inquiry.getInquiryStatus());
        inquiryDto.setInterestedProgram(inquiry.getInterestedProgram());
        if(inquiry.getInquiryManager()!=null){
            inquiryDto.setInquiryManager(new LovDto(inquiry.getInquiryManager().getId(),
                    inquiry.getInquiryManager().getFirstName()+ inquiry.getInquiryManager().getLastName(),""));
        }
        return inquiryDto;
    }

    public User validateInquiryAllowedToUpdate(Integer inquiryId){
        User user;
        Inquiry inquiry = inquiryRepo.findByIdAndActive(inquiryId,true);
        if(inquiry != null) {
            user = helperService.getUserFromSecurityContext();
            if (!(inquiry.getInquiryManager().getId() == user.getId() || user.getUserRole() == UserRole.ADMIN)) {
                throw new IncorrectStateException("Only Inquiry manager or admin user is allowed to update inquiry");
            }else
                return user;
        }else
            throw new IncorrectStateException("No Inquiry exists in the system having id : " + inquiryId);
    }
}
