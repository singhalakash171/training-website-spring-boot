package com.example.trainingwebsite.serviceimpl;


import com.example.trainingwebsite.dto.reviews.TestimonialDto;
import com.example.trainingwebsite.enums.Status;
import com.example.trainingwebsite.model.reviews.InternetReviews;
import com.example.trainingwebsite.model.reviews.Testimonial;
import com.example.trainingwebsite.repository.reviews.InternetReviewsRepository;
import com.example.trainingwebsite.repository.reviews.TestimonialRepository;
import com.example.trainingwebsite.service.ReviewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewsServiceImpl implements ReviewsService {

    @Autowired
    InternetReviewsRepository internetReviewsRepository;

    @Autowired
    TestimonialRepository testimonialRepository;

    public List<InternetReviews> getLatestInternetReviews(){
        return internetReviewsRepository.findAllByActive(true);
    }

    public List<Testimonial> getLatestTestimonials(){
        return testimonialRepository.findAllByActive(true);
    }

    public TestimonialDto createOrUpdateTestimonial(TestimonialDto testimonialDto){
        Testimonial testimonial = new Testimonial();
        testimonial.setStatus(Status.IN_PROGRESS);
        testimonial.setCompanyName(testimonialDto.getCompanyName());
        testimonial.setDescription(testimonialDto.getDescription());
        testimonial.setStatus(testimonialDto.getStatus());
        testimonial.setActive(true);
        testimonial = testimonialRepository.save(testimonial);
        return toTestimonialDto(testimonial);
    }

    public TestimonialDto toTestimonialDto(Testimonial testimonial){
        TestimonialDto testimonialDto = new TestimonialDto();
        testimonialDto.setId(testimonial.getId());
        testimonialDto.setRating(testimonial.getRating());
        testimonialDto.setDescription(testimonial.getDescription());
        testimonialDto.setCompanyName(testimonial.getCompanyName());
        testimonialDto.setStatus(testimonial.getStatus());
        testimonialDto.setImage(testimonial.getImage());
        return testimonialDto;
    }

    public TestimonialDto approveTestimonial(Integer testimonialId){
        Testimonial testimonial = testimonialRepository.findByIdAndActive(testimonialId,true);
        testimonial.setStatus(Status.AVAILABLE);
        testimonial = testimonialRepository.save(testimonial);
        return toTestimonialDto(testimonial);
    }

}
