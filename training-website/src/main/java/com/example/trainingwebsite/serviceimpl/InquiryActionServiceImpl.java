package com.example.trainingwebsite.serviceimpl;

import com.example.trainingwebsite.dto.LovDto;
import com.example.trainingwebsite.dto.inquiry.InquiryActionDetailsDto;
import com.example.trainingwebsite.exception.IncorrectStateException;
import com.example.trainingwebsite.model.inquiry.Inquiry;
import com.example.trainingwebsite.model.inquiry.InquiryAction;
import com.example.trainingwebsite.model.user.User;
import com.example.trainingwebsite.repository.inquiry.InquiryRepo;
import com.example.trainingwebsite.repository.inquiry.InquiryActionRepo;
import com.example.trainingwebsite.service.InquiryService;
import com.example.trainingwebsite.service.InquiryActionService;
import liquibase.util.CollectionUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class InquiryActionServiceImpl implements InquiryActionService {
    @Autowired
    InquiryRepo inquiryRepo;

    @Autowired
    InquiryActionRepo inquiryActionRepo;

    @Autowired
    InquiryService inquiryService;


    @Override
    public InquiryActionDetailsDto createOrUpdateInquiryAction(InquiryActionDetailsDto inquiryActionDetailsDto) {

        LovDto inquiryDto = inquiryActionDetailsDto.getInquiryDto();
        if(inquiryDto == null || inquiryDto.getId() == null)
            throw new IncorrectStateException("InquiryId can't be null");

        User user = inquiryService.validateInquiryAllowedToUpdate(inquiryActionDetailsDto.getInquiryDto().getId());
        Inquiry inquiry = inquiryRepo.findByIdAndActive(inquiryActionDetailsDto.getInquiryDto().getId(),true);
        InquiryAction inquiryAction;

        if(inquiryActionDetailsDto.getId() == null) {
            inquiryAction = new InquiryAction();
            inquiryAction.setComments(inquiryActionDetailsDto.getComments());
            inquiryAction.setLeadAction(inquiryActionDetailsDto.getInquiryActionType());
            inquiryAction.setInquiry(inquiry);
            inquiryAction.setUser(user);
        }
        else{
            inquiryAction = inquiryActionRepo.findByIdAndActive(inquiryActionDetailsDto.getId(),true);
            inquiryAction.setComments(inquiryActionDetailsDto.getComments());
        }
        inquiryAction = inquiryActionRepo.save(inquiryAction);
        return toInquiryActionDetailsDto(inquiryAction);
    }

    public List<InquiryActionDetailsDto> toInquiryActionDetailsDtos(List<InquiryAction> inquiryActionList){

        List<InquiryActionDetailsDto> inquiryActionDetailsDtos = new ArrayList<>();

        if(!CollectionUtils.isEmpty(inquiryActionList)){

            inquiryActionList.stream().forEach(e->{
                inquiryActionDetailsDtos.add(toInquiryActionDetailsDto(e));
            });
        }
        return inquiryActionDetailsDtos;
    }


    public InquiryActionDetailsDto toInquiryActionDetailsDto(InquiryAction inquiryAction){

        InquiryActionDetailsDto inquiryActionDetailsDto = new InquiryActionDetailsDto();
        inquiryActionDetailsDto.setId(inquiryAction.getId());
        inquiryActionDetailsDto.setInquiryDto(new LovDto(inquiryAction.getInquiry().getId(),"",""));
        inquiryActionDetailsDto.setComments(inquiryAction.getComments());
        inquiryActionDetailsDto.setInquiryActionType(inquiryAction.getLeadAction());
        inquiryActionDetailsDto.setActionUserDetails(new LovDto(inquiryAction.getUser().getId(), inquiryAction.getUser().getFirstName() + " " +
                inquiryAction.getUser().getLastName(),""));
        return inquiryActionDetailsDto;
    }

}
