package com.example.trainingwebsite.serviceimpl;

import com.example.trainingwebsite.config.security.AppUserDetails;
import com.example.trainingwebsite.enums.UserRole;
import com.example.trainingwebsite.exception.IncorrectStateException;
import com.example.trainingwebsite.model.user.User;
import com.example.trainingwebsite.repository.user.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
public class HelperService {

    @Autowired
    UserRepo userRepo;

    public User getUserFromSecurityContext(){
        AppUserDetails appUserDetails = (AppUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userEmailId = appUserDetails.getUsername();
        return userRepo.findByEmailIdAndActive(userEmailId,true).get();
    }

    public User getActiveUserById(Integer userId){
        User user;
        if(userId == null)
            throw new IncorrectStateException("UserId/Id is required");
        else {
            user = userRepo.findByIdAndActive(userId, true);
            if(user == null)
                throw new IncorrectStateException("No user exists having id : " + userId);
            else
                return user;
        }
    }

    public User getActiveUserByRole(Integer userId, UserRole userRole){
        User user;
        if(userId == null)
            throw new IncorrectStateException("UserId/Id is required");
        else {
            user = userRepo.findByIdAndUserRoleAndActive(userId,userRole, true);
            if(user == null)
                throw new IncorrectStateException("No user existing having userId : " + userId + " and user role : " + userRole.name());
            else
                return user;
        }
    }

    public User validateCurrentUserHavingRole(UserRole... userRole){
        User user = getUserFromSecurityContext();
        if(user == null)
            throw new IncorrectStateException("User is unable to find in security context...");

        Optional<UserRole> optionalUserRole  = Arrays.stream(userRole).filter(ee-> ee == user.getUserRole()).findFirst();
        if(optionalUserRole.isPresent())
            return user;
        else
            throw new IncorrectStateException("User in current context is does not have : " + userRole + " role");
    }
}
