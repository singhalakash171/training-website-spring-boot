package com.example.trainingwebsite.serviceimpl;


import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.example.trainingwebsite.enums.FileCategory;
import com.example.trainingwebsite.enums.FileSubCategory;
import com.example.trainingwebsite.model.FileData;
import com.example.trainingwebsite.model.training.TrainingFeeProgram;
import com.example.trainingwebsite.model.training.TrainingModule;
import com.example.trainingwebsite.model.training.TrainingProgram;
import com.example.trainingwebsite.model.training.TrainingWhyChoose;
import com.example.trainingwebsite.model.user.User;
import com.example.trainingwebsite.repository.training.TrainingFeeProgramRepo;
import com.example.trainingwebsite.repository.training.TrainingModuleRepository;
import com.example.trainingwebsite.repository.training.TrainingProgramRepository;
import com.example.trainingwebsite.repository.training.WhyChooseRepository;
import com.example.trainingwebsite.repository.user.UserRepo;
import com.example.trainingwebsite.service.AmazonClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AmazonClientServiceServiceImpl implements AmazonClientService {

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;

    @Autowired
    TrainingModuleRepository trainingModuleRepository;

    @Autowired
    TrainingProgramRepository trainingProgramRepository;

    @Autowired
    AmazonClient amazonClient;

    @Autowired
    UserRepo userRepo;

    @Autowired
    WhyChooseRepository whyChooseRepository;

    @Autowired
    TrainingFeeProgramRepo trainingFeeProgramRepo;

    public String uploadFile(FileCategory fileCategory, FileSubCategory subCategory, Integer id, MultipartFile multipartFile){
        String fullFilePath = uploadFile(fileCategory,subCategory,multipartFile);
        String fileRelativeUrl = fullFilePath.replace(endpointUrl,"");
        saveOrUpdateComponent(fileCategory,subCategory,id,fullFilePath,fileRelativeUrl);
        return fullFilePath;
    }

    public String uploadFile(FileCategory fileCategory,FileSubCategory subCategory, MultipartFile multipartFile){
        String fileUrl = "";
        try {
            File file = convertMultiPartToFile(multipartFile);
            String fileName = generateFileName(multipartFile);
            fileUrl = endpointUrl + "/" + fileCategory.bucketName + "/"  + fileName;
            uploadFileTos3bucket(fileCategory,subCategory,fileName, file);
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileUrl;
    }


    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    private String generateFileName(MultipartFile multiPart) {
        return new Date().getTime() + "-" + multiPart.getOriginalFilename().replace(" ", "_");
    }

    private void uploadFileTos3bucket(FileCategory fileCategory, FileSubCategory subCategory,String fileName, File file) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        Map<String,String> additionalData = new HashMap<>();
        additionalData.put("category",fileCategory.name());
        additionalData.put("subCategory",subCategory.name());
        objectMetadata.setUserMetadata(additionalData);
        PutObjectRequest putObjectRequest = new PutObjectRequest(fileCategory.bucketName, fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead);
        putObjectRequest.setMetadata(objectMetadata);
        amazonClient.getS3client().putObject(putObjectRequest);
    }

    public void saveOrUpdateComponent(FileCategory fileCategory, FileSubCategory subCategory, Integer componentId, String fullFilePath,
                                      String fileRelativeUrl){

        switch (fileCategory){

            case TRAINING_MODULE:
               TrainingModule trainingModule =  trainingModuleRepository.findByIdAndActive(componentId,true);
               trainingModule.setImage(fileRelativeUrl);
               trainingModuleRepository.save(trainingModule);
               break;

            case TRAINING:
                TrainingProgram trainingProgram =  trainingProgramRepository.findByIdAndActive(componentId,true);
                if(subCategory == FileSubCategory.SYLLABUS){
                    trainingProgram.setSyllabusFilePath(fileRelativeUrl);
                }else if(subCategory == FileSubCategory.MAIN){
                    trainingProgram.setMainImage(fileRelativeUrl);
                }
                trainingProgramRepository.save(trainingProgram);
                break;

            case USER:
               User user = userRepo.findByIdAndActive(componentId,true);
               user.setProfilePic(fileRelativeUrl);
               userRepo.save(user);
               break;

            case WHY_CHOOSE:
                TrainingWhyChoose trainingWhyChoose = whyChooseRepository.findByIdAndActive(componentId,true);
                trainingWhyChoose.setImage(fileRelativeUrl);
                whyChooseRepository.save(trainingWhyChoose);
                break;

            case FEE_PROGRAM:
                TrainingFeeProgram trainingFeeProgram = trainingFeeProgramRepo.findByIdAndActive(componentId,true);
                trainingFeeProgram.setImage(fileRelativeUrl);
                trainingFeeProgramRepo.save(trainingFeeProgram);
                break;

            default:
                throw new IllegalArgumentException("Invalid file category passed : " + fileCategory);
        }

    }

}
