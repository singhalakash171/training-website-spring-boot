package com.example.trainingwebsite.serviceimpl;


import com.example.trainingwebsite.dto.questions.PageableQuestionDto;
import com.example.trainingwebsite.dto.questions.QuestionDto;
import com.example.trainingwebsite.dto.questions.QuestionModuleDto;
import com.example.trainingwebsite.enums.DifficultyLevel;
import com.example.trainingwebsite.exception.IncorrectStateException;
import com.example.trainingwebsite.model.questions.Question;
import com.example.trainingwebsite.model.questions.QuestionModule;
import com.example.trainingwebsite.enums.Status;
import com.example.trainingwebsite.repository.questions.QuestionRepository;
import com.example.trainingwebsite.repository.questions.QuestionModuleRepository;
import com.example.trainingwebsite.repository.training.TrainingModuleRepository;
import com.example.trainingwebsite.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    TrainingModuleRepository trainingModuleRepository;

    @Autowired
    QuestionModuleRepository questionModuleRepository;

    public QuestionModuleDto createOrUpdateQuestionModule(QuestionModuleDto questionModuleDto){

        if(questionModuleDto.getStatus() == null)
            throw new IncorrectStateException("Status is required ");

        QuestionModule questionModule;
        if(questionModuleDto.getId()!=null){
            questionModule = questionModuleRepository.findByIdAndActive(questionModuleDto.getId(),true);
        }else{
            questionModule = new QuestionModule();
            questionModule.setStatus(Status.AVAILABLE);
        }
        questionModule.setName(questionModuleDto.getName());
        questionModule.setDescription(questionModuleDto.getDescription());
        questionModule.setSequence(questionModuleDto.getSequence());
        questionModule.setActive(true);
        questionModuleRepository.save(questionModule);
        return toQuestionModuleDto(questionModule);
    }

    public List<QuestionModuleDto> getAllQuestionModules(){
        return toQuestionModuleDtos(questionModuleRepository.findAllByActive(true));
    }
    @Override
    public PageableQuestionDto getQuestions(Integer moduleId,String searchText,Pageable pageable) {
        Page<Question> questions;
        PageableQuestionDto pageableQuestionDto = new PageableQuestionDto();

        if(moduleId == null){
            questions = questionRepository.findByQuestionTextContainingAndActive(searchText,true,pageable);
        }else{
            questions = questionRepository.findByQuestionTextContainingAndQuestionModuleIdAndActive
                    (searchText,moduleId,true,pageable);
        }
        pageableQuestionDto.setQuestionDtos(toQuestionDtos(questions.getContent()));
        pageableQuestionDto.setCurrentPage(questions.getNumber());
        pageableQuestionDto.setTotalPages(questions.getTotalPages());
        pageableQuestionDto.setTotalItems(questions.getTotalElements());
        return pageableQuestionDto;
    }

    @Override
    public QuestionDto createOrUpdateQuestion(QuestionDto questionDto) {
        Question question;
        if (questionDto.getId() != null)
            question = questionRepository.findByIdAndActive(questionDto.getId(), true);
        else
            question = new Question();

        if(questionDto.getStatus() == null)
            throw new IncorrectStateException("Status is mandatory");

        question = toQuestionEntity(question,questionDto);
        questionRepository.save(question);
        return toQuestionDto(question);
    }

    @Override
    public QuestionDto approveQuestion(Integer questionId) {
        Question question = questionRepository.findByIdAndActive(questionId, true);
        question.setStatus(Status.AVAILABLE);
        questionRepository.save(question);
        return toQuestionDto(question);
    }

    public List<QuestionDto> toQuestionDtos(List<Question> questions) {
        List<QuestionDto> questionDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(questions)) {
            questions.forEach(e -> {
                QuestionDto questionDto = toQuestionDto(e);
                questionDtos.add(questionDto);
            });
        }
        return questionDtos;
    }

    public QuestionDto toQuestionDto(Question question) {
        QuestionDto questionDto = new QuestionDto();
        questionDto.setId(question.getId());
        questionDto.setQuestion(question.getQuestionText());
        questionDto.setStandardAnswer(question.getStandardAnswer());
        questionDto.setInterviewAnswer(question.getInterviewAnswer());
        questionDto.setDifficultyLevel(question.getDifficultyLevel().toString());
        questionDto.setNoOfTimesAsked(question.getNoOfTimesAsked());
        questionDto.setModuleId(question.getQuestionModule().getId());
        questionDto.setStatus(question.getStatus());
        return questionDto;
    }

    public Question toQuestionEntity(Question question,QuestionDto questionDto) {
        question.setQuestionText(questionDto.getQuestion());
        question.setStandardAnswer(questionDto.getStandardAnswer());
        question.setInterviewAnswer(questionDto.getInterviewAnswer());
        question.setNoOfTimesAsked(questionDto.getNoOfTimesAsked());
        question.setDifficultyLevel(DifficultyLevel.valueOf(questionDto.getDifficultyLevel().toUpperCase()));
        question.setStatus(questionDto.getStatus());
        question.setActive(true);
        QuestionModule questionModule = questionModuleRepository.findByIdAndActive(questionDto.getModuleId(), true);
        question.setQuestionModule(questionModule);
        return question;
    }

    public List<QuestionModuleDto> toQuestionModuleDtos(List<QuestionModule> questionModules) {
        List<QuestionModuleDto> trainingModuleDtos = new ArrayList<>();
        questionModules.forEach(e->{
            trainingModuleDtos.add(toQuestionModuleDto(e));
        });
        return trainingModuleDtos;
    }

    public QuestionModuleDto toQuestionModuleDto(QuestionModule questionModule){
        QuestionModuleDto questionModuleDto = new QuestionModuleDto();
        questionModuleDto.setId(questionModule.getId());
        questionModuleDto.setName(questionModule.getName());
        questionModuleDto.setDescription(questionModule.getDescription());
        questionModuleDto.setSequence(questionModule.getSequence());
        questionModuleDto.setStatus(questionModule.getStatus());
        return questionModuleDto;
    }

}
