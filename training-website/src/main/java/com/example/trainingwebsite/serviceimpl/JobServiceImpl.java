package com.example.trainingwebsite.serviceimpl;


import com.example.trainingwebsite.model.placement.JobDetails;
import com.example.trainingwebsite.model.placement.JobPlacementDetails;
import com.example.trainingwebsite.enums.Status;
import com.example.trainingwebsite.repository.job.JobRepository;
import com.example.trainingwebsite.repository.job.PlacementRepository;
import com.example.trainingwebsite.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobServiceImpl implements JobService {

    @Autowired
    JobRepository jobRepository;

    @Autowired
    PlacementRepository placementRepository;

    @Override
    public JobDetails addOrUpdateJobDetails(JobDetails jobDetailsData) {
        JobDetails jobDetails;
        if (jobDetailsData.getId() != null) {
            jobDetails = jobRepository.findByActiveAndId(true, jobDetailsData.getId());
        }else{
            jobDetails = new JobDetails();
        }
        jobDetails.setCompanyWebsiteUrl(jobDetailsData.getCompanyWebsiteUrl());
        jobDetails.setJobCurrentStatus(jobDetailsData.getJobCurrentStatus());
        jobDetails.setStatus(Status.IN_PROGRESS);
        jobDetails.setEligibilityCriteria(jobDetailsData.getEligibilityCriteria());
        jobDetails.setJobType(jobDetailsData.getJobType());
        jobDetails.setJobCategory(jobDetailsData.getJobCategory());
        jobDetails.setTitle(jobDetailsData.getTitle());
        jobDetails.setNoOfVacancies(jobDetailsData.getNoOfVacancies());
        jobDetails = jobRepository.save(jobDetails);
        return jobDetails;
    }

    @Override
    public List<JobDetails> getAllJobDetails() {
        return jobRepository.findAllByActiveAndStatus(true, Status.AVAILABLE);
    }

    @Override
    public JobDetails approvePostedJob(Integer jobId) {
        JobDetails jobDetails = jobRepository.findByActiveAndId(true, jobId);
        jobDetails.setStatus(Status.AVAILABLE);
        return jobDetails;
    }

    public JobPlacementDetails addOrUpdatePlacementDetails(JobPlacementDetails jobPlacementDetailsData){

        JobPlacementDetails jobPlacementDetails;
        if (jobPlacementDetailsData.getId() != null) {
            jobPlacementDetails = placementRepository.findByIdAndActive( jobPlacementDetailsData.getId(),true);
        }else{
            jobPlacementDetails = new JobPlacementDetails();
        }

        jobPlacementDetails.setActive(true);
        jobPlacementDetails.setDate(jobPlacementDetailsData.getDate());
        jobPlacementDetails.setCompanyName(jobPlacementDetailsData.getCompanyName());
        jobPlacementDetails.setStudentName(jobPlacementDetailsData.getStudentName());
        jobPlacementDetails.setDescription(jobPlacementDetailsData.getDescription());
        jobPlacementDetails = placementRepository.save(jobPlacementDetails);
        return jobPlacementDetails;
    }

    @Override
    public JobPlacementDetails approvePlacement(Integer placementId) {
        JobPlacementDetails jobPlacementDetails = placementRepository.findByIdAndActive(placementId,true);
        jobPlacementDetails.setStatus(Status.AVAILABLE);
        return jobPlacementDetails;
    }

}
