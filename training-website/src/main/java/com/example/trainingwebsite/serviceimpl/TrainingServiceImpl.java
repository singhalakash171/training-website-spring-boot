package com.example.trainingwebsite.serviceimpl;


import com.example.trainingwebsite.dto.training.*;
import com.example.trainingwebsite.enums.Status;
import com.example.trainingwebsite.enums.UserRole;
import com.example.trainingwebsite.exception.IncorrectStateException;
import com.example.trainingwebsite.model.training.*;
import com.example.trainingwebsite.model.user.User;
import com.example.trainingwebsite.repository.training.*;
import com.example.trainingwebsite.repository.user.UserRepo;
import com.example.trainingwebsite.service.TrainingService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrainingServiceImpl implements TrainingService {

    @Autowired
    TrainingProgramRepository trainingProgramRepository;

    @Autowired
    UserRepo userRepo;

    @Autowired
    HelperService helperService;

    @Autowired
    TrainingModuleRepository trainingModuleRepository;

    @Autowired
    TrainingTopicRepository trainingTopicRepository;

    @Autowired
    TrainingFaqRepository trainingFaqRepository;

    @Autowired
    TrainingFeaturesRepository trainingFeaturesRepository;

    @Autowired
    WhyChooseRepository whyChooseRepository;

    @Autowired
    TrainingCriteriaRepository trainingCriteriaRepository;

    @Autowired
    TrainingFeeProgramRepo trainingFeeProgramRepo;


    @Override
    public TrainingProgramDto createOrUpdateTrainingBasicDetails(TrainingProgramDto trainingProgramDto) {

        if(trainingProgramDto.getTrainingCategory() == null || trainingProgramDto.getStatus() == null)
            throw new IncorrectStateException("Training category/Status is required");

        TrainingProgram trainingProgram;
        if (trainingProgramDto.getId() != null) {
            trainingProgram = trainingProgramRepository.findByIdAndActive(trainingProgramDto.getId(), true);
        } else {
            trainingProgram = new TrainingProgram();
        }
        trainingProgram.setName(trainingProgramDto.getName());
        trainingProgram.setDescription(trainingProgramDto.getDescription());
        trainingProgram.setTrainingCategory(trainingProgramDto.getTrainingCategory());
        trainingProgram.setActive(true);
        trainingProgram.setSequence(trainingProgramDto.getSequence());
        trainingProgram.setOverview(trainingProgramDto.getOverview());
        trainingProgram.setStatus(trainingProgramDto.getStatus() != null ? trainingProgramDto.getStatus() : Status.DRAFT);
        trainingProgram = trainingProgramRepository.save(trainingProgram);
        return toTrainingProgramDto(trainingProgram);
    }

    @Override
    public TrainingProgramDto approveTraining(Integer trainingId) {
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(trainingId, true);
        trainingProgram.setStatus(Status.AVAILABLE);
        trainingProgramRepository.save(trainingProgram);
        return toTrainingProgramDto(trainingProgram);
    }

    @Override
    @Transactional
    public TrainingModuleDto createOrUpdateModuleDetail(Integer trainingId, TrainingModuleDto trainingModuleDto) {
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(trainingId, true);
        TrainingModule trainingModule;
        if (trainingModuleDto.getId() != null) {
            trainingModule = trainingModuleRepository.findByIdAndActive(trainingModuleDto.getId(), true);
        } else {
            trainingModule = new TrainingModule();
        }
        trainingModule.setActive(true);
        trainingModule.setName(trainingModuleDto.getName());
        trainingModule.setDescription(trainingModuleDto.getDescription());
        trainingModule.setStatus(trainingModuleDto.getStatus() != null ? trainingModuleDto.getStatus() : Status.DRAFT);
        trainingModule.setSequence(trainingModuleDto.getSequence());
        trainingModule.setTrainingProgram(trainingProgram);
        trainingModuleRepository.save(trainingModule);
        return toTrainingModuleDto(trainingModule, false);
    }

    @Override
    public TrainingModuleDto approveModule(Integer moduleId) {
        TrainingModule trainingModule = trainingModuleRepository.findByIdAndActive(moduleId, true);
        trainingModule.setStatus(Status.AVAILABLE);
        trainingModuleRepository.save(trainingModule);
        return toTrainingModuleDto(trainingModule, false);
    }

    @Override
    @Transactional
    public TrainingTopicDto createOrUpdateTrainingTopic(Integer moduleId, TrainingTopicDto trainingTopicDto) {
        TrainingModule trainingModule = trainingModuleRepository.findByIdAndActive(moduleId, true);
        TrainingTopic trainingTopic;
        if (trainingTopicDto.getId() != null) {
            trainingTopic = trainingTopicRepository.findByIdAndActive(trainingTopicDto.getId(), true);
        } else {
            trainingTopic = new TrainingTopic();
        }
        trainingTopic.setActive(true);
        trainingTopic.setName(trainingTopicDto.getName());
        trainingTopic.setDescription(trainingTopicDto.getDescription());
        trainingTopic.setStatus(trainingTopicDto.getStatus() != null ? trainingTopicDto.getStatus() : Status.DRAFT);
        trainingTopic.setSequence(trainingTopicDto.getSequence());
        trainingTopic.setTrainingModule(trainingModule);
        trainingTopic = trainingTopicRepository.save(trainingTopic);
        return toTrainingTopicDto(trainingTopic);
    }

    @Override
    public TrainingTopicDto approveTopics(Integer topicId) {
        TrainingTopic trainingTopic = trainingTopicRepository.findByIdAndActive(topicId, true);
        trainingTopic.setStatus(Status.AVAILABLE);
        trainingTopicRepository.save(trainingTopic);
        return toTrainingTopicDto(trainingTopic);
    }

    @Override
    @Transactional
    public TrainingFeatureDto createOrUpdateTrainingFeatures(Integer trainingId, TrainingFeatureDto trainingFeatureDto) {
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(trainingId, true);
        TrainingFeature trainingFeature;
        if (trainingFeatureDto.getId() != null) {
            trainingFeature = trainingFeaturesRepository.findByIdAndActiveOrderBySequenceAsc(trainingFeatureDto.getId(), true);
        } else {
            trainingFeature = new TrainingFeature();
        }
        trainingFeature.setActive(true);
        trainingFeature.setFeature(trainingFeatureDto.getName());
        trainingFeature.setDescription(trainingFeatureDto.getDescription());
        trainingFeature.setStatus(trainingFeatureDto.getStatus() != null ? trainingFeatureDto.getStatus() : Status.DRAFT);
        trainingFeature.setTrainingProgram(trainingProgram);
        trainingFeature.setSequence(trainingFeatureDto.getSequence());
        trainingFeaturesRepository.save(trainingFeature);
        return toTrainingFeatureDto(trainingFeature);
    }


    @Override
    @Transactional
    public TrainingCriteriaDto createOrUpdateTrainingCriteria(Integer trainingId, TrainingCriteriaDto trainingCriteriaDto) {
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(trainingId, true);
        TrainingCriteria trainingCriteria;
        if (trainingCriteriaDto.getId() != null) {
            trainingCriteria = trainingCriteriaRepository.findByIdAndActive(trainingCriteriaDto.getId(), true);
        } else {
            trainingCriteria = new TrainingCriteria();
        }
        trainingCriteria.setActive(true);
        trainingCriteria.setCriteria(trainingCriteriaDto.getCriteriaName());
        trainingCriteria.setStatus(trainingCriteriaDto.getStatus() != null ? trainingCriteriaDto.getStatus() : Status.DRAFT);
        trainingCriteria.setCriteriaDescription(trainingCriteriaDto.getCriteriaDescription());
        trainingCriteria.setSequence(trainingCriteriaDto.getSequence());
        trainingCriteria.setTrainingProgram(trainingProgram);
        trainingCriteriaRepository.save(trainingCriteria);
        return toTrainingCriteriaDto(trainingCriteria);
    }

    @Override
    public TrainingCriteriaDto approveTrainingCriteria(Integer trainingCriteriaId) {
        TrainingCriteria trainingCriteria = trainingCriteriaRepository.findByIdAndActive(trainingCriteriaId, true);
        trainingCriteria.setStatus(Status.AVAILABLE);
        trainingCriteriaRepository.save(trainingCriteria);
        return toTrainingCriteriaDto(trainingCriteria);
    }

    public List<TrainingCriteriaDto> getAllTrainingCriteriaList(Integer trainingId) {
        List<TrainingCriteria> trainingCriteriaList = trainingCriteriaRepository.findAllByTrainingProgramIdAndActiveOrderBySequenceAsc(trainingId, true);
        return toTrainingCriteriaDtos(trainingCriteriaList);
    }

    @Transactional
    public void linkTrainingProgramToTrainer(Integer trainingProgramId, Integer trainerId) {
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(trainingProgramId, true);
        if (trainingProgram != null) {
            helperService.validateCurrentUserHavingRole(UserRole.TRAINER, UserRole.ADMIN);
            User user = helperService.getActiveUserByRole(trainerId, UserRole.TRAINER);
            trainingProgram.getTrainers().add(user);
            trainingProgramRepository.save(trainingProgram);
        } else
            throw new IncorrectStateException("Training Program having Id : " + trainingProgramId + "  does not exists");
    }

    @Override
    @Transactional
    public WhyChooseDto createOrUpdateTrainingWhyChoose(Integer trainingId, WhyChooseDto whyChooseDto) {
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(trainingId, true);
        TrainingWhyChoose trainingWhyChoose;
        if (whyChooseDto.getId() != null) {
            trainingWhyChoose = whyChooseRepository.findByIdAndActive(whyChooseDto.getId(), true);
        } else {
            trainingWhyChoose = new TrainingWhyChoose();
        }
        trainingWhyChoose.setActive(true);
        trainingWhyChoose.setName(whyChooseDto.getName());
        trainingWhyChoose.setStatus(whyChooseDto.getStatus() != null ? whyChooseDto.getStatus() : Status.DRAFT);
        trainingWhyChoose.setDescription(whyChooseDto.getDescription());
        trainingWhyChoose.setSequence(whyChooseDto.getSequence());
        trainingWhyChoose.setTrainingProgram(trainingProgram);
        whyChooseRepository.save(trainingWhyChoose);
        return toWhyChooseDto(trainingWhyChoose);
    }

    @Override
    public WhyChooseDto approveTrainingWhyChoose(Integer whyChooseId) {
        TrainingWhyChoose trainingWhyChoose = whyChooseRepository.findByIdAndActive(whyChooseId, true);
        trainingWhyChoose.setStatus(Status.AVAILABLE);
        whyChooseRepository.save(trainingWhyChoose);
        return toWhyChooseDto(trainingWhyChoose);
    }

    public List<WhyChooseDto> getTrainingWhyChoose(Integer trainingId) {
        List<TrainingWhyChoose> trainingBenefits = whyChooseRepository.findAllByTrainingProgramIdAndActiveOrderBySequenceAsc(trainingId, true);
        return toWhyChooseDtos(trainingBenefits);
    }

    @Override
    public TrainingFeatureDto approveFeature(Integer trainingFaqId) {
        TrainingFeature trainingFeature = trainingFeaturesRepository.findByIdAndActiveOrderBySequenceAsc(trainingFaqId, true);
        trainingFeature.setStatus(Status.AVAILABLE);
        trainingFeaturesRepository.save(trainingFeature);
        return toTrainingFeatureDto(trainingFeature);
    }

    @Override
    public List<TrainingFeatureDto> getTrainingFeatures(Integer trainingId) {
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(trainingId, true);
        return toTrainingFeatureDtos(trainingProgram.getTrainingFeatures());
    }

    @Override
    public TrainingFaqDto createOrUpdateTrainingFaq(Integer trainingId, TrainingFaqDto trainingFaqDto) {
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(trainingId, true);
        TrainingFaq trainingFaq;
        if (trainingFaqDto.getId() != null) {
            trainingFaq = trainingFaqRepository.findByIdAndActiveOrderBySequenceAsc(trainingFaqDto.getId(), true);
        } else {
            trainingFaq = new TrainingFaq();
        }
        trainingFaq.setActive(true);
        trainingFaq.setQuestion(trainingFaqDto.getQuestion());
        trainingFaq.setAnswer(trainingFaqDto.getAnswer());
        trainingFaq.setStatus(trainingFaqDto.getStatus() != null ? trainingFaqDto.getStatus() : Status.DRAFT);
        trainingFaq.setTrainingProgram(trainingProgram);
        trainingFaq.setSequence(trainingFaqDto.getSequence());
        trainingFaqRepository.save(trainingFaq);
        return toFaqQuestionDto(trainingFaq);
    }

    public List<TrainingFaqDto> getFaqDetails(Integer programId) {
        List<TrainingFaqDto>  trainingFaqDtos = new ArrayList<>();
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(programId, true);
        if (trainingProgram != null) {
            if (!CollectionUtils.isEmpty(trainingProgram.getTrainingFaqs())) {
                trainingFaqDtos = toFaqQuestionDtos(trainingProgram.getTrainingFaqs());
            }
        }else{
            throw new IncorrectStateException("No program exists having program id : " + programId);
        }
        return trainingFaqDtos;
    }

    @Override
    public TrainingFaqDto approveFaqQuestion(Integer trainingFaqId) {
        TrainingFaq trainingFaq = trainingFaqRepository.findByIdAndActiveOrderBySequenceAsc(trainingFaqId, true);
        trainingFaq.setStatus(Status.AVAILABLE);
        trainingFaqRepository.save(trainingFaq);
        return toFaqQuestionDto(trainingFaq);
    }

    @Override
    @Transactional
    public TrainingFeeProgramDto createTrainingFeeProgram(TrainingFeeProgramDto trainingFeeProgramDto, Integer trainingId) {

        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(trainingId, true);
        TrainingFeeProgram trainingFeeProgram;
        if (trainingFeeProgramDto.getId() != null) {
            trainingFeeProgram = trainingFeeProgramRepo.findByIdAndActiveOrderBySequenceAsc(trainingFeeProgramDto.getId(), true);
        } else {
            trainingFeeProgram = new TrainingFeeProgram();
        }
        trainingFeeProgram.setTrainingFee(trainingFeeProgramDto.getProgramFee());
        trainingFeeProgram.setDiscountedFee(trainingFeeProgramDto.getDiscountedFee());
        trainingFeeProgram.setDescription(trainingFeeProgramDto.getDescription());
        trainingFeeProgram.setFeatures(trainingFeeProgramDto.getFeatures());
        trainingFeeProgram.setStatus(trainingFeeProgramDto.getStatus() != null ? trainingFeeProgramDto.getStatus() : Status.DRAFT);
        trainingFeeProgram.setSequence(trainingFeeProgramDto.getSequence());
        trainingFeeProgram.setActive(true);
        trainingFeeProgram.setTrainingProgram(trainingProgram);
        trainingFeeProgramRepo.save(trainingFeeProgram);
        return toTrainingFeeProgramDto(trainingFeeProgram);
    }

    public TrainingFeeProgramDto approveFeeProgram(Integer programId) {
        TrainingFeeProgram trainingFeeProgram = trainingFeeProgramRepo.findByIdAndActiveOrderBySequenceAsc(programId, true);
        trainingFeeProgram.setStatus(Status.AVAILABLE);
        trainingFeeProgramRepo.save(trainingFeeProgram);
        return toTrainingFeeProgramDto(trainingFeeProgram);
    }

    public List<TrainingFeeProgramDto> getTrainingFeeProgramDetails(Integer programId) {
        List<TrainingFeeProgramDto>  trainingFeeProgramDtos = new ArrayList<>();
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(programId, true);
        if (trainingProgram != null) {
            if (!CollectionUtils.isEmpty(trainingProgram.getTrainingFeePrograms())) {
                trainingFeeProgramDtos = toTrainingFeeProgramDtos(trainingProgram.getTrainingFeePrograms());
            }
        }else{
            throw new IncorrectStateException("No program exists having program id : " + programId);
        }
        return trainingFeeProgramDtos;
    }

    public List<TrainingFeeProgramDto> toTrainingFeeProgramDtos(List<TrainingFeeProgram> trainingFeePrograms) {

        List<TrainingFeeProgramDto> trainingFeeProgramDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(trainingFeePrograms)) {
            trainingFeePrograms.forEach(e -> {
                trainingFeeProgramDtos.add(toTrainingFeeProgramDto(e));
            });
        }
        return trainingFeeProgramDtos;
    }

    public TrainingFeeProgramDto toTrainingFeeProgramDto(TrainingFeeProgram trainingFeeProgram) {
        TrainingFeeProgramDto trainingFeeProgramDto = new TrainingFeeProgramDto();
        trainingFeeProgramDto.setId(trainingFeeProgram.getId());
        trainingFeeProgramDto.set_id(trainingFeeProgram.getId().toString());
        trainingFeeProgramDto.setStatus(trainingFeeProgram.getStatus());
        trainingFeeProgramDto.setDescription(trainingFeeProgram.getDescription());
        trainingFeeProgramDto.setFeatures(trainingFeeProgram.getFeatures());
        trainingFeeProgramDto.setProgramFee(trainingFeeProgram.getTrainingFee());
        trainingFeeProgramDto.setDiscountedFee(trainingFeeProgram.getDiscountedFee());
        trainingFeeProgramDto.setSequence(trainingFeeProgram.getSequence());
        return trainingFeeProgramDto;
    }

    @Override
    public List<TrainingProgramDto> getAllTraining() {
        return toTrainingProgramDtos(trainingProgramRepository.findAllByActiveOrderBySequenceAsc(true));
    }

    @Override
    public List<TrainingProgramDto> getTrainingByCategory(TrainingCategory trainingCategory) {
        if (trainingCategory != null) {
            return toTrainingProgramDtos(trainingProgramRepository.findAllByActiveAndTrainingCategory(true, trainingCategory));
        } else {
            return getAllTraining();
        }
    }

    @Override
    @Transactional
    public TrainingProgramDetailsDto getTrainingProgramDetails(Integer programId) {
        TrainingProgramDetailsDto trainingProgramDetailsDto = new TrainingProgramDetailsDto();
        TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(programId, true);
        if (trainingProgram != null) {
            trainingProgramDetailsDto.setTrainingProgramDto(toTrainingProgramDto(trainingProgram));
            trainingProgramDetailsDto.setTrainingModuleDtoList(toTrainingModuleDtos(trainingProgram.getTrainingModules(), true));
        }

        if (!CollectionUtils.isEmpty(trainingProgram.getTrainingFaqs())) {
            trainingProgramDetailsDto.setTrainingFaqQuestion(toFaqQuestionDtos(trainingProgram.getTrainingFaqs()));
        }

        if (!CollectionUtils.isEmpty(trainingProgram.getTrainingFeatures())) {
            trainingProgramDetailsDto.setTrainingFeatureDtos(toTrainingFeatureDtos(trainingProgram.getTrainingFeatures()));
        }

        if (!CollectionUtils.isEmpty(trainingProgram.getTrainingFaqs())) {
            trainingProgramDetailsDto.setTrainingFaqQuestion(toFaqQuestionDtos(trainingProgram.getTrainingFaqs()));
        }

        if (!CollectionUtils.isEmpty(trainingProgram.getTrainingFeePrograms())) {
            trainingProgramDetailsDto.setTrainingFeeProgramDtos(toTrainingFeeProgramDtos(trainingProgram.getTrainingFeePrograms()));
        }

        if (!CollectionUtils.isEmpty(trainingProgram.getTrainingCriteriaList())) {
            trainingProgramDetailsDto.setTrainingCriteriaDtos(toTrainingCriteriaDtos(trainingProgram.getTrainingCriteriaList()));
        }


        return trainingProgramDetailsDto;
    }

    public List<TrainingProgramDto> toTrainingProgramDtos(List<TrainingProgram> trainingPrograms) {
        List<TrainingProgramDto> trainingProgramDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(trainingPrograms)) {
            trainingPrograms.forEach(e -> {
                trainingProgramDtos.add(toTrainingProgramDto(e));
            });
        }
        return trainingProgramDtos;
    }

    public TrainingProgramDto toTrainingProgramDto(TrainingProgram trainingProgram) {
        TrainingProgramDto trainingProgramDto = new TrainingProgramDto();
        trainingProgramDto.setId(trainingProgram.getId());
        trainingProgramDto.set_id(trainingProgram.getId().toString());
        trainingProgramDto.setName(trainingProgram.getName());
        trainingProgramDto.setDescription(trainingProgram.getDescription());
        trainingProgramDto.setStatus(trainingProgram.getStatus());
        trainingProgramDto.setTrainingCategory(trainingProgram.getTrainingCategory());
        trainingProgramDto.setSequence(trainingProgram.getSequence());
        trainingProgramDto.setOverview(trainingProgram.getOverview());
        trainingProgramDto.setMainImage(trainingProgram.getMainImage());
        trainingProgramDto.setSyllabusFilePath(trainingProgram.getSyllabusFilePath());
        return trainingProgramDto;
    }

    @Transactional
    public List<TrainingModuleDto> getModules(Integer programId, boolean includeTopics) {
        List<TrainingModuleDto> trainingModuleDtos = new ArrayList<>();
        if (programId != null) {
            TrainingProgram trainingProgram = trainingProgramRepository.findByIdAndActive(programId, true);
            if (trainingProgram != null) {
                trainingModuleDtos = toTrainingModuleDtos(trainingProgram.getTrainingModules(), true);
            }
        } else {
            trainingModuleDtos = toTrainingModuleDtos(trainingModuleRepository.findAllByActive(true), includeTopics);
        }
        return trainingModuleDtos;
    }

    public List<TrainingModuleDto> toTrainingModuleDtos(List<TrainingModule> trainingModules, boolean includeTopicsDetail) {
        List<TrainingModuleDto> trainingModuleDtos = new ArrayList<>();
        trainingModules.forEach(e -> {
            trainingModuleDtos.add(toTrainingModuleDto(e, includeTopicsDetail));
        });
        return trainingModuleDtos;
    }

    public TrainingModuleDto toTrainingModuleDto(TrainingModule trainingModule, boolean includeTopicsDetail) {
        TrainingModuleDto trainingModuleDto = new TrainingModuleDto();
        trainingModuleDto.setId(trainingModule.getId());
        trainingModuleDto.set_id(trainingModule.getId().toString());
        trainingModuleDto.setName(trainingModule.getName());
        trainingModuleDto.setDescription(trainingModule.getDescription());
        trainingModuleDto.setSequence(trainingModule.getSequence());
        trainingModuleDto.setStatus(trainingModule.getStatus());
        trainingModuleDto.setImage(trainingModule.getImage());
        if (includeTopicsDetail) {
            trainingModuleDto.setTrainingTopicDtoList(toTrainingTopicsDtos(trainingModule.getTrainingTopics()));
        }
        return trainingModuleDto;
    }

    public List<TrainingTopicDto> toTrainingTopicsDtos(List<TrainingTopic> trainingTopics) {
        List<TrainingTopicDto> trainingTopicDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(trainingTopics)) {
            trainingTopics.forEach(e -> {
                trainingTopicDtos.add(toTrainingTopicDto(e));
            });
        }
        return trainingTopicDtos;
    }

    public TrainingTopicDto toTrainingTopicDto(TrainingTopic trainingTopic) {
        TrainingTopicDto trainingTopicDto = new TrainingTopicDto();
        trainingTopicDto.setId(trainingTopic.getId());
        trainingTopicDto.set_id(trainingTopic.getId().toString());
        trainingTopicDto.setName(trainingTopic.getName());
        trainingTopicDto.setDescription(trainingTopic.getDescription());
        trainingTopicDto.setStatus(trainingTopic.getStatus());
        trainingTopicDto.setSequence(trainingTopic.getSequence());
        return trainingTopicDto;
    }

    public List<TrainingFaqDto> toFaqQuestionDtos(List<TrainingFaq> trainingFaqs) {
        List<TrainingFaqDto> questionDtos = new ArrayList<>();

        if (!CollectionUtils.isEmpty(trainingFaqs)) {
            trainingFaqs.forEach(e -> {
                questionDtos.add(toFaqQuestionDto(e));
            });
        }
        return questionDtos;
    }

    public TrainingFaqDto toFaqQuestionDto(TrainingFaq trainingFaq) {
        TrainingFaqDto trainingFaqDto = new TrainingFaqDto();
        trainingFaqDto.setId(trainingFaq.getId());
        trainingFaqDto.set_id(trainingFaq.getId().toString());
        trainingFaqDto.setQuestion(trainingFaq.getQuestion());
        trainingFaqDto.setAnswer(trainingFaq.getAnswer());
        trainingFaqDto.setStatus(trainingFaq.getStatus());
        trainingFaqDto.setSequence(trainingFaq.getSequence());
        return trainingFaqDto;
    }

    public List<WhyChooseDto> toWhyChooseDtos(List<TrainingWhyChoose> trainingBenefits) {
        List<WhyChooseDto> whyChooseDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(trainingBenefits)) {
            trainingBenefits.forEach(e -> {
                whyChooseDtos.add(toWhyChooseDto(e));
            });
        }
        return whyChooseDtos;
    }

    public WhyChooseDto toWhyChooseDto(TrainingWhyChoose trainingWhyChoose) {
        WhyChooseDto whyChooseDto = new WhyChooseDto();
        whyChooseDto.setId(trainingWhyChoose.getId());
        whyChooseDto.setStatus(trainingWhyChoose.getStatus());
        whyChooseDto.setName(trainingWhyChoose.getName());
        whyChooseDto.setSequence(trainingWhyChoose.getSequence());
        return whyChooseDto;
    }

    public List<TrainingCriteriaDto> toTrainingCriteriaDtos(List<TrainingCriteria> trainingCriteriaList) {
        List<TrainingCriteriaDto> trainingCriteriaDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(trainingCriteriaList)) {
            trainingCriteriaList.forEach(e -> {
                trainingCriteriaDtos.add(toTrainingCriteriaDto(e));
            });
        }
        return trainingCriteriaDtos;
    }

    public TrainingCriteriaDto toTrainingCriteriaDto(TrainingCriteria trainingCriteria) {
        TrainingCriteriaDto trainingCriteriaDto = new TrainingCriteriaDto();
        trainingCriteriaDto.setId(trainingCriteria.getId());
        trainingCriteriaDto.set_id(trainingCriteria.getId());
        trainingCriteriaDto.setStatus(trainingCriteria.getStatus());
        trainingCriteriaDto.setCriteriaName(trainingCriteria.getCriteria());
        trainingCriteriaDto.setCriteriaDescription(trainingCriteria.getCriteriaDescription());
        trainingCriteriaDto.setSequence(trainingCriteria.getSequence());
        return trainingCriteriaDto;
    }

    public List<TrainingFeatureDto> toTrainingFeatureDtos(List<TrainingFeature> trainingFeatures) {
        List<TrainingFeatureDto> trainingFeatureDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(trainingFeatures)) {
            trainingFeatures.forEach(e -> {
                trainingFeatureDtos.add(toTrainingFeatureDto(e));
            });
        }
        return trainingFeatureDtos;
    }

    public TrainingFeatureDto toTrainingFeatureDto(TrainingFeature trainingFeature) {
        TrainingFeatureDto trainingFeatureDto = new TrainingFeatureDto();
        trainingFeatureDto.setId(trainingFeature.getId());
        trainingFeatureDto.set_id(trainingFeature.getId());
        trainingFeatureDto.setStatus(trainingFeature.getStatus());
        trainingFeatureDto.setName(trainingFeature.getDescription());
        trainingFeatureDto.setDescription(trainingFeature.getFeature());
        trainingFeatureDto.setSequence(trainingFeature.getSequence());
        return trainingFeatureDto;
    }

//    @Override
//    public List<TrainingScheduleDto> getTrainingProgramSchedule(Integer programId) {
//        List<TrainingSchedule> trainingSchedules;
//        if (programId != null) {
//            trainingSchedules = trainingScheduleRepository.findAllByActiveAndTrainingProgramId(true, programId);
//        } else {
//            trainingSchedules = trainingScheduleRepository.findAllByActive(true);
//        }
//        return getTrainingScheduleDtos(trainingSchedules);
//    }

    public List<TrainingScheduleDto> getTrainingScheduleDtos(List<TrainingSchedule> trainingSchedules) {
        List<TrainingScheduleDto> trainingScheduleDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(trainingSchedules)) {

            trainingSchedules.forEach(e -> {
                TrainingScheduleDto trainingScheduleDto = new TrainingScheduleDto();
                trainingScheduleDto.setId(e.getId());
                trainingScheduleDto.setDate(e.getTrainingDate());
                trainingScheduleDto.setEndTime(e.getEndTime());
                trainingScheduleDto.setStartTime(e.getEndTime());
                trainingScheduleDto.setDuration(e.getDuration());
                trainingScheduleDtos.add(trainingScheduleDto);
            });
        }
        return trainingScheduleDtos;
    }
}
